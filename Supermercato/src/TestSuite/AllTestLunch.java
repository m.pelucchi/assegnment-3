package TestSuite;
import testservice.AllServiceTests;
import tests.AllTests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class AllTestLunch {
	static JUnitCore junitCore;

	public static void main(String[] args) {
		System.out.println("Start operation tests");
		JUnitCore runner = new JUnitCore();
		runner.addListener(new CustomExecutionListener());
		Result result = runner.run(AllTests.class);
		if (result.wasSuccessful())
			System.out.println("Operations tests passed successfully");
		
		System.out.println(" ");
		
		System.out.println("Start service tests");
		Result result1 = runner.run(AllServiceTests.class);
		if (result1.wasSuccessful())
			System.out.println("Services tests passed successfully");
		
		if(result.wasSuccessful() && result1.wasSuccessful())
			System.out.println(" ");
			System.out.println("End of test: All tests passed successfully");
	}
}