package entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;

import exceptions.NotValidCfException;
import exceptions.NotValidEmploymentException;
  
@Entity(name="Butcher")
public class Butcher extends Employee {
	
	@Column(name="\"employment\"")
	private String employment;
	
	public Butcher() {
		super ();
	}

	public Butcher(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Employee boss, Supermarket supermarket, Collection<Product> products,String employment) throws NotValidCfException, NotValidEmploymentException {
		
		super(cf,name,surname,day, mounth, year,schedule,salary,boss,supermarket,products);
		this.setEmployment(employment);
	}

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) throws NotValidEmploymentException {
		if(employment == null)
			throw new NotValidEmploymentException();
		else
			this.employment = employment;
	}

	@Override
	public String toString() {
		return "Butcher [cf=\" + cf + \", name=\" + name + \", surname=\" + surname + \", date=\" + date\r\n" + 
				"+ \", schedule=\" + schedule + \", salary=\" + salary + \", boss=\" + boss + \", employee=\" + employee\r\n" + 
				"+ \", supermarket=\" + supermarket + \", products=\" + products + employment=" + employment + "]";
	}
	
	
	
	
}
