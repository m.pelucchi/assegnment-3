package entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import exceptions.NotValidCashNumberException;
import exceptions.NotValidCfException;

@Entity(name="Cashier")  
@Inheritance(strategy=InheritanceType.JOINED)
public class Cashier extends Employee {
	
	@Column(name="\"cash number\"")
	private int cashNumber;
	
	public Cashier() {
		super ();
		
	}

	public Cashier(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Employee boss, Supermarket supermarket, Collection<Product> products,int cashNumber) throws NotValidCfException, NotValidCashNumberException {
		
		super(cf,name,surname,day,mounth,year,schedule,salary,boss,supermarket,products);
		this.setCashNumber(cashNumber);
	}

	public int getCashNumber() {
		return cashNumber;
	}

	public void setCashNumber(int cashNumber) throws NotValidCashNumberException {
		if(cashNumber == 0 || cashNumber > 40)
			throw new NotValidCashNumberException();
		else
			this.cashNumber = cashNumber;
	}

	@Override
	public String toString() {
		return "Cashier [cf=\" + cf + \", name=\" + name + \", surname=\" + surname + \", date=\" + date\r\n" + 
				"+ \", schedule=\" + schedule + \", salary=\" + salary + \", boss=\" + boss + \", employee=\" + employee\r\n" + 
				"+ \", supermarket=\" + supermarket + \", products=\" + products + cashNumber=" + cashNumber + "]";
	}
	
}
