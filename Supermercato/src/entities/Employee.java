package entities;

import java.util.Calendar;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import exceptions.NotValidCfException;

@Entity(name="Employee")
@Inheritance(strategy=InheritanceType.JOINED)
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="\"cf\"")
	private String cf;
		
	@Column(name="name", nullable = false)
	private String name;
	@Column(name="surname", nullable = false)
	private String surname;
	
	@Column(name="bornDate", nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar date;
	
	@Column(name="schedule")//Orario
	private int schedule;
	
	@Column(name="salary")
	private double salary;
		
	@ManyToOne
	private Employee boss;
	
    @OneToMany(mappedBy="boss")
    @CascadeOnDelete
    private Collection<Employee> employees;
	
    @ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="supermarket", referencedColumnName="\"Id\"")
	private Supermarket supermarket;
			
	@ManyToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST}, fetch = FetchType.LAZY)
	@JoinTable(name = "\"Order\"",
	        joinColumns = @JoinColumn(name = "\"CfEmployee\""),
	        inverseJoinColumns = @JoinColumn(name = "\"ProductId\"")
	    )
	private Collection<Product> products;
	
	//Constructor
	public Employee() {
		super();
	}

	public Employee(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Employee boss, Supermarket supermarket, Collection<Product> products) throws NotValidCfException {
		this.setCf(cf);
		this.setName(name);
		this.setSurname(surname);
		this.setDate(day,mounth,year);
		this.setSchedule(schedule);
		this.setSalary(salary);
		this.setBoss(boss);
		this.setSupermarket(supermarket);
		this.setProducts(products);
	}
	
	
	//Getters and Setters
	public String getCf() {
		return cf;
	}

	public void setCf(String codf) throws NotValidCfException {
		if( codf == null) {
			throw new NotValidCfException("The tax code can not be empty");
		} else {
			ControllCF(codf);
			this.cf=codf;
		}		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(int day, int month, int year) {
		date = Calendar.getInstance();
	    date.set(Calendar.DAY_OF_MONTH, day);
	    date.set(Calendar.MONTH, month-1);
	    date.set(Calendar.YEAR, year);
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}

	public int getSchedule() {
		return schedule;
	}

	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Employee getBoss() {
		return boss;
	}

	public void setBoss(Employee boss) {
		this.boss = boss;
	}
	
	public Collection<Employee> getEmployee() {
		return employees;
	}

	public void setEmployee(Collection<Employee> employee) {
		this.employees = employee;
	}

	public Supermarket getSupermarket() {
		return supermarket;
	}

	public void setSupermarket(Supermarket supermarket) {
		this.supermarket = supermarket;
	}

	public Collection<Product> getProducts() {
		return products;
	}

	public void setProducts(Collection<Product> products) {
		this.products = products;
	}
	
	public void setEmployee(Employee employee) throws NotValidCfException {
		this.setName(employee.name);
		this.setSurname(employee.surname);
		this.setCf(employee.cf);
		this.setDate(employee.date);
		this.setSchedule(employee.schedule);
		this.setSalary(employee.salary);
		this.setBoss(employee.boss);
		this.setSupermarket(employee.supermarket);
		this.setProducts(employee.products);		
	}

	
	public String ControllCF(String cf) throws NotValidCfException {
		int i, s, c;
		String cf2;
		int setdisp[] = {1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20,
			11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
		if( cf.length() == 0 ) 
			throw new NotValidCfException("The tax code can not be empty");
		if( cf.length() != 16 )
			throw new NotValidCfException("the length of the tax code is not correct: the tax code should be exactly 16 characters long.");
		cf2 = cf.toUpperCase();
		for( i=0; i<16; i++ ){
			c = cf2.charAt(i);
			if( ! ( c>='0' && c<='9' || c>='A' && c<='Z' ) )
				throw new NotValidCfException("The tax code contains invalid characters: the only valid characters are letters and digits");
		}
		s = 0;
		for( i=1; i<=13; i+=2 ){
			c = cf2.charAt(i);
			if( c>='0' && c<='9' )
				s = s + c - '0';
			else
				s = s + c - 'A';
		}
		for( i=0; i<=14; i+=2 ){
			c = cf2.charAt(i);
			if( c>='0' && c<='9' )	 c = c - '0' + 'A';
			s = s + setdisp[c - 'A'];
		}
		if( s%26 + 'A' != cf2.charAt(15) )
			throw new NotValidCfException("The tax code is incorrect: the control code does not match");
		return cf;
	}
	
	@Override
	public String toString() {
		return "Employee [cf=" + cf + ", name=" + name + ", surname=" + surname + ", date=" + date
				+ ", schedule=" + schedule + ", salary=" + salary + ", boss=" + boss + ", employee=" + employees
				+ ", supermarket=" + supermarket + ", products=" + products + "]";
	}
	
}

