package entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Product {
	@Id @Column(name="\"id\"")
	private int id;
	
	@Column(name="name")
	String name;
	
	@Column(name="description")
	String description;
	  
	@Column(name="weight")
	int weight;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "products")
	private Collection<Employee> employees;
	
	@ManyToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST})
	@JoinTable(name = "location",
	        joinColumns = @JoinColumn(name = "ProductId"),
	        inverseJoinColumns = @JoinColumn(name = "WarehousId")
	    )
	private Collection<Warehouse> warehouses;
	
	
	public Product() {
		super();
	}

	public Product(int id, String name, String description, int weight,
			Collection<Warehouse> warehouses) {
		super();
		this.setId(id);
		this.setName(name);
		this.setDescription(description);
		this.setWeight(weight);
		//this.employees = employees;
		this.setWarehouses(warehouses);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Collection<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Collection<Employee> employees) {
		this.employees = employees;
	}

	public Collection<Warehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(Collection<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}

	public void setProduct(Product product) {
		this.setId(id);
		this.setName(product.name);
		this.setDescription(product.description);
		this.setWeight(product.weight);
		this.setEmployees(product.employees);
		this.setWarehouses(product.warehouses);		
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", weight=" + weight
				+ ", employees=" + employees + ", warehouses=" + warehouses + "]";
	}
	
	
}