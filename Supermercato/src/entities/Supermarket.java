package entities;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="Supermarket") 
public class Supermarket {
	
	@Id @Column(name="Id")
	private int id;
	
	@Column(name="\"name\"")
	String name;
	
	@Column(name="\"city\"")
	String city;
	
	@Column(name="\"typology\"")
	String typology;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy= "supermarket")
	private Collection <Employee> employees;
	
	public Supermarket() {
		super();
		employees = new ArrayList<Employee>();
	}

	public Supermarket(int id, String name, String city, String typology, Collection<Employee> employees) {
		super();
		this.setId(id);
		this.setName(name);
		this.setCity(city);
		this.setTypology(typology);
		this.setEmployees(employees);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public Collection<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Collection<Employee> employees) {
		this.employees = employees;
	}
	
	public void setSupermarket(Supermarket supermarket) {
		this.setId(supermarket.id);
		this.setName(supermarket.name);
		this.setCity(supermarket.city);
		this.setTypology(supermarket.typology);
		this.setEmployees(supermarket.employees);	
	}
	
	
}


