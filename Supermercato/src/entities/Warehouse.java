package entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity  
public class Warehouse {
         
    @Id @Column(name="Id")
	private int id;
    
    @ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="supermarket", referencedColumnName="Id")
	private Supermarket supermarket;
    
    public Supermarket getSupermercato() {
		return supermarket;
	}

	public void setSupermercato(Supermarket supermarket) {
		this.supermarket = supermarket;
	}
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "warehouses")
    private  Collection<Product> products;
	
    
    @Column(name="name")
	private String name;
    
    @Column(name="city")
	private String city;
    
    public Warehouse (){
    	super();
    }

	public Warehouse(int id, String name, String city, Supermarket supermarket, Collection<Product> products) {
		super();
		this.setId(id);
		this.setName(name);
		this.setCity(city);
		this.setSupermarket(supermarket);
		this.setProducts(products);
	}
	
	//Getters and setters	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Supermarket getSupermarket() {
		return supermarket;
	}

	public void setSupermarket(Supermarket supermarket) {
		this.supermarket = supermarket;
	}

	public Collection<Product> getProducts() {
		return products;
	}

	public void setProducts(Collection<Product> products) {
		this.products = products;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Warehouse [id=" + id + ", supermarket=" + supermarket + ", products=" + products + ", name=" + name
				+ ", city=" + city + "]";
	}

	public void setWarehouse(Warehouse warehouse) {
		this.setId(warehouse.id);
		this.setSupermarket(supermarket);
		this.setProducts(products);
		this.setName(name);
		this.setCity(city);
	}
        
}