package exceptions;

public class AlreadyExistException extends Exception {
			
	private static final long serialVersionUID = 1L;

	public AlreadyExistException() {
		super("already exist");
	}

	public AlreadyExistException(String m) {
		super(m);
	}


}
