package exceptions;

public class NotFoundException extends Exception {
			
	private static final long serialVersionUID = 1L;

	public NotFoundException() {
		super("No occurrence found");
	}

	public NotFoundException(String m) {
		super(m);
	}


}
