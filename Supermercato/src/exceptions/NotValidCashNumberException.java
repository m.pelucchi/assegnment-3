package exceptions;

public class NotValidCashNumberException extends Exception{
	private static final long serialVersionUID = 2L;
	
	public NotValidCashNumberException() {
		super("CashNumber could not be 0");
	}

	public NotValidCashNumberException(String message) {
		super(message);
	}
	
}
