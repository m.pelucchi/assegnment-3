package exceptions;

public class NotValidCfException extends Exception{
	private static final long serialVersionUID = 2L;
	
	public NotValidCfException() {
		super("Error");
	}

	public NotValidCfException(String message) {
		super(message);
	}
	
}
