package exceptions;

public class NotValidEmploymentException extends Exception{
	private static final long serialVersionUID = 2L;
	
	public NotValidEmploymentException() {
		super("Employment could not be empty");
	}

	public NotValidEmploymentException(String message) {
		super(message);
	}
	
}
