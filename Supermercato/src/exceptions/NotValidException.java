package exceptions;

public class NotValidException extends Exception {
			
	private static final long serialVersionUID = 1L;

	public NotValidException() {
		super("Field empty");
	}

	public NotValidException(String m) {
		super(m);
	}


}
