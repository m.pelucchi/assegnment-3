package exceptions;

public class UpdateException extends Exception {
			
	private static final long serialVersionUID = 1L;

	public UpdateException() {
		super("Update failed");
	}

	public UpdateException(String m) {
		super(m);
	}


}
