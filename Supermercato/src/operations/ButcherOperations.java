package operations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Butcher;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;


public class ButcherOperations {
	
	//Create new 
	public static void addNew(Butcher butcher, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
			if(em.find(Butcher.class, butcher.getCf()) == null) {
				em.persist(butcher);
				em.getTransaction().commit();
			} else {
				throw new AlreadyExistException("Butcher already exist");
			}
	}
	
	// Search (by codice fiscale)
	public static Butcher findOccurence(String cf, EntityManager em) throws NotFoundException {
		Butcher temp = em.find(Butcher.class, cf);
		if (temp == null)
			throw new NotFoundException("Butcher not found");
		return temp;
	}

	//Update
	public static void updateOccurence(Butcher butcher, EntityManager em) throws NotFoundException, NotValidCfException {
		em.getTransaction().begin();
		Butcher tmp = findOccurence(butcher.getCf(), em);
		tmp.setEmployee(butcher);
		em.getTransaction().commit();
	}

	//Delete
	public static void delete(String cf, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Butcher tmp = findOccurence(cf, em);
		try {
			tmp = findOccurence(cf, em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		
		Query q = em.createQuery("SELECT s FROM Supermarket s JOIN Butcher e ON e.cf = :cf");
		q.setParameter("cf", cf);
		List<Supermarket> supers = (List<Supermarket>) q.getResultList();
		if(supers != null) {
			for (Supermarket supermarket : supers) {
				supermarket.getEmployees().remove(tmp);
			}
		}
		q = em.createQuery("SELECT e FROM Butcher e INNER JOIN e.boss c WHERE c.cf = :cf");
		q.setParameter("cf", cf);
		List<Butcher> butchers = (List<Butcher>) q.getResultList();
		if(butchers != null) {
			for (Butcher b : butchers) {
				b.setBoss(null);
			}
		}
		tmp.setBoss(null);
		em.remove(tmp);
		em.getTransaction().commit();
	}
	
}
