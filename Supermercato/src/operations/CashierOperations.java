package operations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Cashier;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;


public class CashierOperations {
	
	//Create new 
	public static void addNew(Cashier cashier, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
			if(em.find(Cashier.class, cashier.getCf()) == null) {
				em.persist(cashier);
				em.getTransaction().commit();
			} else {
				throw new AlreadyExistException("Cashier already exist");
			}
	}
	
	// Search (by codice fiscale)
	public static Cashier findOccurence(String cf, EntityManager em) throws NotFoundException {
		Cashier temp = em.find(Cashier.class, cf);
		if (temp == null)
			throw new NotFoundException("Cashier not found");
		return temp;
	}

	//Update
	public static void updateOccurence(Cashier cashier, EntityManager em) throws NotFoundException, NotValidCfException {
		em.getTransaction().begin();
		Cashier tmp = findOccurence(cashier.getCf(), em);
		tmp.setEmployee(cashier);
		em.getTransaction().commit();
	}

	//Delete
	public static void delete(String cf, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Cashier tmp = findOccurence(cf, em);
		try {
			tmp = findOccurence(cf, em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		
		Query q = em.createQuery("SELECT s FROM Supermarket s JOIN Cashier e ON e.cf = :cf");
		q.setParameter("cf", cf);
		List<Supermarket> supers = (List<Supermarket>) q.getResultList();
		if(supers != null) {
			for (Supermarket supermarket : supers) {
				supermarket.getEmployees().remove(tmp);
			}
		}
		q = em.createQuery("SELECT e FROM Cashier e INNER JOIN e.boss c WHERE c.cf = :cf");
		q.setParameter("cf", cf);
		List<Cashier> cashiers = (List<Cashier>) q.getResultList();
		if(cashiers != null) {
			for (Cashier c : cashiers) {
				c.setBoss(null);
			}
		}
		tmp.setBoss(null);
		em.remove(tmp);
		em.getTransaction().commit();
	}
	
}
