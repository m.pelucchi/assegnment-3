package operations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Employee;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;


public class EmployeeOperations {
	
	//Create new 
	public static void addNew(Employee employee, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
			if(em.find(Employee.class, employee.getCf()) == null) {
				em.persist(employee);
				em.getTransaction().commit();
			} else {
				throw new AlreadyExistException("Employee already exist");
			}
	}
	
	// Search (by codice fiscale)
	public static Employee findOccurence(String cf, EntityManager em) throws NotFoundException {
		Employee temp = em.find(Employee.class, cf);
		if (temp == null)
			throw new NotFoundException("Employee not found");
		return temp;
	}

	//Update
	public static void updateOccurence(Employee employee, EntityManager em) throws NotFoundException, NotValidCfException {
		em.getTransaction().begin();
		Employee tmp = null;
		try {
			tmp = findOccurence(employee.getCf(), em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw new NotFoundException();
		}
		tmp.setEmployee(employee);
		em.merge(tmp);
		em.getTransaction().commit();
	}
	

	//Delete
	public static void delete(String cf, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Employee tmp = findOccurence(cf, em);
		try {
			tmp = findOccurence(cf, em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		
		Query q = em.createQuery("SELECT s FROM Supermarket s JOIN Employee e ON e.cf = :cf");
		q.setParameter("cf", cf);
		List<Supermarket> supers = (List<Supermarket>) q.getResultList();
		if(supers != null) {
			for (Supermarket supermarket : supers) {
				supermarket.getEmployees().remove(tmp);
			}
		}
		q = em.createQuery("SELECT e FROM Employee e INNER JOIN e.boss c WHERE c.cf = :cf");
		q.setParameter("cf", cf);
		List<Employee> dip = (List<Employee>) q.getResultList();
		if(dip != null) {
			for (Employee dipe : dip) {
				dipe.setBoss(null);
			}
		}
		tmp.setBoss(null);
		em.remove(tmp);
		em.getTransaction().commit();
	}

	
}
