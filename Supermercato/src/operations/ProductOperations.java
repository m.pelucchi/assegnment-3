package operations;

import javax.persistence.EntityManager;

import entities.Product;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;

public class ProductOperations {
	
	//Create new 
	public static void addNew(Product product, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
			if(em.find(Product.class, product.getId()) == null) {
				em.persist(product);
				em.getTransaction().commit();
			} else {
				throw new AlreadyExistException("Product already exist");
			}
	}
	
	// Search (by codice fiscale)
	public static Product findOccurence(int id, EntityManager em) throws NotFoundException {
		Product temp = em.find(Product.class, id);
		if (temp == null)
			throw new NotFoundException("Product not found");
		return temp;
	}

	//Update
	public static void updateOccurence(Product product, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Product tmp = findOccurence(product.getId(), em);
		tmp.setProduct(product);
		em.getTransaction().commit();
	}

	//Delete
	public static void delete(int id, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Product tmp = findOccurence(id, em);
		em.remove(tmp);
		em.getTransaction().commit();
	}
	
}
