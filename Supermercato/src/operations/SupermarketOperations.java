package operations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Employee;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;


public class SupermarketOperations {
	
	//Create new 
	public static void addNew(Supermarket supermarket, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
		if(em.find(Supermarket.class, supermarket.getId()) == null) {
			em.persist(supermarket);
			em.getTransaction().commit();
		} else {
			throw new AlreadyExistException("Supermarket already exist");
		}
	}
	
	// Search (by id)
	public static Supermarket findOccurence(int id, EntityManager em) throws NotFoundException {
		Supermarket temp = em.find(Supermarket.class, id);
		if (temp == null)
			throw new NotFoundException();
		return temp;
	}

	//Update
	public static void updateOccurence(Supermarket supermarket, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Supermarket tmp = findOccurence(supermarket.getId(), em);
		tmp.setSupermarket(supermarket);
		em.getTransaction().commit();
	}

	//Delete
	public static void delete(int id, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Supermarket tmp = findOccurence(id, em);
		try {
			tmp = findOccurence(id, em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		//SELECT e FROM Employee e JOIN Supermarket s ON s.id = :id  
	
		Query q = em.createQuery("SELECT e FROM Employee e INNER JOIN e.supermarket s WHERE s.id = :id");
		q.setParameter("id", id);
		List<Employee> employees = (List<Employee>) q.getResultList();
		if(employees != null) {
			for (Employee employee : employees) {
				employee.setSupermarket(null);
			}
		}
		q = em.createQuery("SELECT w FROM Warehouse w INNER JOIN w.supermarket s WHERE s.id = :id");
		q.setParameter("id", id);
		List<Warehouse> warehs = (List<Warehouse>) q.getResultList();
		if(warehs != null) {
			for (Warehouse w : warehs) {
				w.setSupermarket(null);
			}
		}
		
		em.remove(tmp);
		em.getTransaction().commit();
	}
}
