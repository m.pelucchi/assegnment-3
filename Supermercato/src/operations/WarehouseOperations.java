package operations;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Product;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;


public class WarehouseOperations {
	
	//Create new 
	public static void addNew(Warehouse warehouse, EntityManager em) throws AlreadyExistException {
		em.getTransaction().begin();
			if(em.find(Warehouse.class, warehouse.getId()) == null) {
				em.persist(warehouse);
				em.getTransaction().commit();
			} else {
				throw new AlreadyExistException("Warehouse already exist");
			}
	}
	
	// Search (by id)
	public static Warehouse findOccurence(int id, EntityManager em) throws NotFoundException {
		Warehouse temp = em.find(Warehouse.class, id);
		if (temp == null)
			throw new NotFoundException("Warehouse not found");
		return temp;
	}

	//Update
	public static void updateOccurence(Warehouse warehouse, EntityManager em) throws NotFoundException, NotValidCfException {
		em.getTransaction().begin();
		Warehouse tmp = null;
		try {
			tmp = findOccurence(warehouse.getId(), em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setWarehouse(warehouse);
		em.getTransaction().commit();
	}

	//Delete
		public static void delete(int id, EntityManager em) throws NotFoundException {
		em.getTransaction().begin();
		Warehouse tmp = findOccurence(id, em);
		try {
			tmp = findOccurence(id, em);
		}
		catch(NotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		
		Query q = em.createQuery("SELECT p FROM Product p JOIN Warehouse w ON w.id = :id");
		q.setParameter("id", id);
		List<Product> products = (List<Product>) q.getResultList();
		if(products != null) {
			for (Product prod : products) {
				prod.getWarehouses().remove(tmp);
			}
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}

	
}
