package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Butcher;
import entities.Employee;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;
import exceptions.NotValidEmploymentException;
import exceptions.NotValidException;
import exceptions.UpdateException;
import operations.ButcherOperations;
import operations.EmployeeOperations;
import operations.ProductOperations;
import operations.SupermarketOperations;

public class ServiceButcher {
	
	private static Butcher insertButcher(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Butcher boss, Supermarket supermarket, Collection<Product> products, String employment, EntityManager em) throws AlreadyExistException, NotValidCfException, NotValidException, NotValidEmploymentException{
		Butcher e = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(surname.equals("")) {
				throw new NotValidException("Surname field is empty, insert one");
			}
			if(day <= 0 || day >= 32 || mounth < 0 || mounth >= 13 || year < 0 || year > Calendar.getInstance().get(Calendar.YEAR)) {
				throw new NotValidException("BornDate is not correct");
			}
			if(employment.equals("")) {
				throw new NotValidEmploymentException();
			}
			e = new Butcher(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, products, employment);
			ButcherOperations.addNew(e, em);
			if(supermarket != null) {
				UpdateSupermarketButcher(e, supermarket);
			}
		}
		catch(PersistenceException ex) { 
			em.detach(e);
			return null; 
		}
		return e;
	}
	
	private static void UpdateSupermarketButcher(Butcher e, Supermarket s) {
		Collection<Employee> employees = s.getEmployees();
		if(!employees.contains(e)) {
			employees.add(e);
			s.setEmployees(employees);
		}
	}
		
	public static Butcher insertButcher(String cf, String name, String surname, int day, 
			int mounth, int year, String employment, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidEmploymentException {
				return insertButcher(cf, name, surname, day, mounth, year, 0, 0, null, null, null, employment, em);
	}
	
	public static Butcher insertButcher(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, String employment, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidEmploymentException {
				return insertButcher(cf, name, surname, day, mounth, year, schedule, salary, null, null, null, employment, em);
	}
	
	public static Butcher insertButcher(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int Sid, String employment, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidEmploymentException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertButcher(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, null, employment, em);
	}
			
	public static Butcher insertButcher(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, String Ecf, int Sid, String employment, EntityManager em)
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidEmploymentException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				Butcher boss = ButcherOperations.findOccurence(Ecf, em);
				return insertButcher(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, null, employment, em);
	}
		
	public static Butcher insertButcher(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int[] IDproducts,
			 int Sid, String employment, EntityManager em) throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidEmploymentException {
				Collection <Product> products = new ArrayList<Product>();
				for(int i= 0; i < IDproducts.length; i++) {
					products.add(ProductOperations.findOccurence(IDproducts[i], em));
				}
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertButcher(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, products, employment, em);
	}
	
	public static Butcher findButcher(String cf, EntityManager em) throws NotFoundException {
		return ButcherOperations.findOccurence(cf, em);
	}
	
	private static Butcher updateAtt(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException, NotValidEmploymentException {
		Butcher e = ButcherOperations.findOccurence(cf, em);
		if(m == null) return e;
		if(m.isEmpty()) return e;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				e.setName(m.get(key).toString());
			}
			else if("surname".equalsIgnoreCase(key)) {
				e.setSurname(m.get(key).toString());
			}
			else if("date".equalsIgnoreCase(key)) {
				e.setDate((Calendar)(m.get(key)));
			}
			else if("schedule".equalsIgnoreCase(key)) {
				e.setSchedule((int)m.get(key));
			}
			else if("salary".equalsIgnoreCase(key)) {
				e.setSalary((int)m.get(key));
			}
			else if("employment".equalsIgnoreCase(key)) {
				e.setEmployment(m.get(key).toString());
			}
		}
		return e;
	}
	
	
	private static boolean pushUpdated(Butcher e, EntityManager em) throws UpdateException, NotFoundException, NotValidCfException {
		try {
			ButcherOperations.updateOccurence(e, em);
		}
		catch(NotFoundException ex) {
			return false;
		}
		return true;
	}

	public static boolean updateButcher(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidEmploymentException{
		Butcher e = updateAtt(cf, m, em);
		return pushUpdated(e, em);
	}
	
	public static boolean updateButcher(String cf, HashMap<String, Object> m, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidEmploymentException{
		Butcher e = updateAtt(cf, m, em);
		e.setSupermarket(supermarket);
		UpdateSupermarketButcher(e, supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateButcher(String cf, HashMap<String, Object> m, Butcher boss, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidEmploymentException{
		Butcher e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		UpdateSupermarketButcher(e, supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateButcher(String cf, HashMap<String, Object> m, Butcher boss, Supermarket supermarket, Collection<Product> products, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidEmploymentException{
		Butcher e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		UpdateSupermarketButcher(e, supermarket);
		e.setProducts(products);
		return pushUpdated(e, em);
	}
	
	public static boolean deleteButcher(String cf, EntityManager em) {
		try {
			ButcherOperations.delete(cf, em);
		}
		catch(NotFoundException e){
			return false;
		}
		return true;
	}
	
	public static Collection<Butcher> searchBySupermarket(Supermarket supermarket, EntityManager em) {
		Query q = em.createQuery("SELECT b FROM Butcher b WHERE b.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Butcher>) q.getResultList();
	}
	
	public static Collection<Butcher> searchBySupermarket(int id, EntityManager em) throws NotFoundException {
		Supermarket supermarket = SupermarketOperations.findOccurence(id, em);
		Query q = em.createQuery("SELECT b FROM Butcher b WHERE b.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Butcher>) q.getResultList();
	}
	
	public static Collection<Butcher> searchByBoss(Employee boss, EntityManager em) {
		Query q = em.createQuery("SELECT b FROM Employee b WHERE b.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Butcher>) q.getResultList();
	}
	
	public static Collection<Butcher> searchByBoss(String cf, EntityManager em) throws NotFoundException {
		Employee boss = EmployeeOperations.findOccurence(cf, em);
		Query q = em.createQuery("SELECT b FROM Employee b WHERE b.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Butcher>) q.getResultList();
	}
	
	public static Collection<Butcher> searchBySalary(double min, double max, EntityManager em){
		Query q = em.createQuery("SELECT b FROM Butcher b");
		Collection<Butcher> result = (Collection<Butcher>) q.getResultList();
		Collection<Butcher> returns = new ArrayList<Butcher>();
		result.forEach((temp) -> {
            if(temp.getSalary()< max && temp.getSalary()> min) {
            	returns.add(temp);
            };
		});
		return returns;
	}


}
