package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Cashier;
import entities.Employee;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCashNumberException;
import exceptions.NotValidCfException;
import exceptions.NotValidException;
import exceptions.UpdateException;
import operations.CashierOperations;
import operations.EmployeeOperations;
import operations.ProductOperations;
import operations.SupermarketOperations;

public class ServiceCashier {
	
	private static Cashier insertCashier(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Cashier boss, Supermarket supermarket, Collection<Product> products, int cashNumber, EntityManager em) throws AlreadyExistException, NotValidCfException, NotValidException, NotValidCashNumberException{
		Cashier e = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(surname.equals("")) {
				throw new NotValidException("Surname field is empty, insert one");
			}
			if(day <= 0 || day >= 32 || mounth < 0 || mounth >= 13 || year < 0 || year > Calendar.getInstance().get(Calendar.YEAR)) {
				throw new NotValidException("BornDate is not correct");
			}
			if(cashNumber == 0) {
				throw new NotValidCashNumberException();
			}
			e = new Cashier(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, products, cashNumber);
			CashierOperations.addNew(e, em);
			if(supermarket != null) {
				UpdateSupermarketCashier(e, supermarket);
			}
		}
		catch(PersistenceException ex) { 
			em.detach(e);
			return null; 
		}
		return e;
	}
	
	private static void UpdateSupermarketCashier(Cashier c, Supermarket s) {
		Collection<Employee> employees = s.getEmployees();
		if(!employees.contains(c)) {
			employees.add(c);
			s.setEmployees(employees);
		}
	}
	
	public static Cashier insertCashier(String cf, String name, String surname, int day, 
			int mounth, int year, int cashNumber, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidCashNumberException {
				return insertCashier(cf, name, surname, day, mounth, year, 0, 0, null, null, null, cashNumber, em);
	}
	
	public static Cashier insertCashier(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int cashNumber, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidCashNumberException {
				return insertCashier(cf, name, surname, day, mounth, year, schedule, salary, null, null, null, cashNumber, em);
	}
	
	public static Cashier insertCashier(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int Sid, int cashNumber, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidCashNumberException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertCashier(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, null, cashNumber, em);
	}
			
	public static Cashier insertCashier(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, String Ecf, int Sid, int cashNumber, EntityManager em)
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidCashNumberException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				Cashier boss = CashierOperations.findOccurence(Ecf, em);
				return insertCashier(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, null, cashNumber, em);
	}
		
	public static Cashier insertCashier(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int[] IDproducts,
			 int Sid, int cashNumber, EntityManager em) throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException, NotValidCashNumberException {
				Collection <Product> products = new ArrayList<Product>();
				for(int i= 0; i < IDproducts.length; i++) {
					products.add(ProductOperations.findOccurence(IDproducts[i], em));
				}
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertCashier(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, products, cashNumber, em);
	}
	
	public static Cashier findCashier(String cf, EntityManager em) throws NotFoundException {
		return CashierOperations.findOccurence(cf, em);
	}
	
	private static Cashier updateAtt(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException, NotValidCashNumberException {
		Cashier e = CashierOperations.findOccurence(cf, em);
		if(m == null) return e;
		if(m.isEmpty()) return e;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				e.setName(m.get(key).toString());
			}
			else if("surname".equalsIgnoreCase(key)) {
				e.setSurname(m.get(key).toString());
			}
			else if("date".equalsIgnoreCase(key)) {
				e.setDate((Calendar)(m.get(key)));
			}
			else if("schedule".equalsIgnoreCase(key)) {
				e.setSchedule((int)m.get(key));
			}
			else if("salary".equalsIgnoreCase(key)) {
				e.setSalary((int)m.get(key));
			}
			else if("cashNumber".equalsIgnoreCase(key)) {
				e.setCashNumber((int)m.get(key));
			}
		}
		return e;
	}
	
	
	private static boolean pushUpdated(Cashier e, EntityManager em) throws UpdateException, NotFoundException, NotValidCfException {
		try {
			CashierOperations.updateOccurence(e, em);
		}
		catch(NotFoundException ex) {
			return false;
		}
		return true;
	}

	public static boolean updateCashier(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidCashNumberException{
		Cashier e = updateAtt(cf, m, em);
		return pushUpdated(e, em);
	}
	
	public static boolean updateCashier(String cf, HashMap<String, Object> m, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidCashNumberException{
		Cashier e = updateAtt(cf, m, em);
		e.setSupermarket(supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateCashier(String cf, HashMap<String, Object> m, Cashier boss, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidCashNumberException{
		Cashier e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateCashier(String cf, HashMap<String, Object> m, Cashier boss, Supermarket supermarket, Collection<Product> products, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException, NotValidCashNumberException{
		Cashier e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		e.setProducts(products);
		return pushUpdated(e, em);
	}
	
	public static boolean deleteCashier(String cf, EntityManager em) {
		try {
			CashierOperations.delete(cf, em);
		}
		catch(NotFoundException e){
			return false;
		}
		return true;
	}
	
	public static Collection<Cashier> searchBySupermarket(Supermarket supermarket, EntityManager em) {
		Query q = em.createQuery("SELECT c FROM Cashier c WHERE c.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Cashier>) q.getResultList();
	}
	
	public static Collection<Cashier> searchBySupermarket(int id, EntityManager em) throws NotFoundException {
		Supermarket supermarket = SupermarketOperations.findOccurence(id, em);
		Query q = em.createQuery("SELECT c FROM Cashier c WHERE c.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Cashier>) q.getResultList();
	}
	
	public static Collection<Cashier> searchByBoss(Employee boss, EntityManager em) {
		Query q = em.createQuery("SELECT c FROM Cashier c WHERE c.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Cashier>) q.getResultList();
	}
	
	public static Collection<Cashier> searchByBoss(String cf, EntityManager em) throws NotFoundException {
		Employee boss = CashierOperations.findOccurence(cf, em);
		Query q = em.createQuery("SELECT e FROM Cashier e WHERE e.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Cashier>) q.getResultList();
	}
		
	public static Collection<Cashier> searchBySalary(double min, double max, EntityManager em){
		Query q = em.createQuery("SELECT c FROM Cashier c");
		Collection<Cashier> result = (Collection<Cashier>) q.getResultList();
		Collection<Cashier> returns = new ArrayList<Cashier>();
		result.forEach((temp) -> {
            if(temp.getSalary()< max && temp.getSalary()> min) {
            	returns.add(temp);
            };
		});
		return returns;
	}


}
