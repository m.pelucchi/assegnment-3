package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Employee;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;
import exceptions.NotValidException;
import exceptions.UpdateException;
import operations.EmployeeOperations;
import operations.ProductOperations;
import operations.SupermarketOperations;

public class ServiceEmployee {
	
	private static Employee insertEmployee(String cf, String name, String surname, int day, int mounth, int year, int schedule, double salary,
			Employee boss, Supermarket supermarket, Collection<Product> products, EntityManager em) throws AlreadyExistException, NotValidCfException, NotValidException{
		Employee e = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(surname.equals("")) {
				throw new NotValidException("Surname field is empty, insert one");
			}
			if(day <= 0 || day >= 32 || mounth < 0 || mounth >= 13 || year < 0 || year > Calendar.getInstance().get(Calendar.YEAR)) {
				throw new NotValidException("BornDate is not correct");
			}
			e = new Employee(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, products);
			EmployeeOperations.addNew(e, em);
			if(supermarket != null) {
				UpdateSupermarketEmployee(e, supermarket);
			}
		}
		catch(PersistenceException ex) { 
			em.detach(e);
			return null; 
		}
		return e;
	}
	
	private static void UpdateSupermarketEmployee(Employee e, Supermarket s) {
		Collection<Employee> employees = s.getEmployees();
		if(!employees.contains(e)) {
			employees.add(e);
			s.setEmployees(employees);
		}
	}
	
	public static Employee insertEmployee(String cf, String name, String surname, int day, 
			int mounth, int year, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException {
				return insertEmployee(cf, name, surname, day, mounth, year, 0, 0, null, null, null, em);
	}
	
	public static Employee insertEmployee(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException {
				return insertEmployee(cf, name, surname, day, mounth, year, schedule, salary, null, null, null, em);
	}
	
	public static Employee insertEmployee(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int Sid, EntityManager em) 
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertEmployee(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, null, em);
	}
			
	public static Employee insertEmployee(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, String Ecf, int Sid, EntityManager em)
			throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException {
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				Employee boss = EmployeeOperations.findOccurence(Ecf, em);
				if(boss.getSupermarket()!=null && boss.getSupermarket()!= supermarket)
					throw new NotValidCfException("Boss and Employee must works in the same supermarket");
				return insertEmployee(cf, name, surname, day, mounth, year, schedule, salary, boss, supermarket, null,em);
	}
		
	public static Employee insertEmployee(String cf, String name, String surname, int day, 
			int mounth, int year, int schedule, double salary, int[] IDproducts,
			 int Sid, EntityManager em) throws AlreadyExistException, NotValidCfException, NotFoundException, NotValidException {
				Collection <Product> products = new ArrayList<Product>();
				for(int i = 0; i < IDproducts.length; i++) {
					products.add(ProductOperations.findOccurence(IDproducts[i], em));
				}
				Supermarket supermarket = SupermarketOperations.findOccurence(Sid, em);
				return insertEmployee(cf, name, surname, day, mounth, year, schedule, salary, null, supermarket, products,em);
	}
	
	public static Employee findEmployee(String cf, EntityManager em) throws NotFoundException {
		return EmployeeOperations.findOccurence(cf, em);
	}
	
	private static Employee updateAtt(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException {
		Employee e = EmployeeOperations.findOccurence(cf, em);
		if(m == null) return e;
		if(m.isEmpty()) return e;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				e.setName(m.get(key).toString());
			}
			else if("surname".equalsIgnoreCase(key)) {
				e.setSurname(m.get(key).toString());
			}
			else if("date".equalsIgnoreCase(key)) {
				e.setDate((Calendar) m.get(key));
			}
			else if("schedule".equalsIgnoreCase(key)) {
				e.setSchedule((int)m.get(key));
			}
			else if("salary".equalsIgnoreCase(key)) {
				e.setSalary((int)m.get(key));
			}
		}
		return e;
	}
	
	
	private static boolean pushUpdated(Employee e, EntityManager em) throws UpdateException, NotFoundException, NotValidCfException {
		try {
			EmployeeOperations.updateOccurence(e, em);
		}
		catch(NotFoundException ex) {
			return false;
		}
		return true;
	}

	public static boolean updateEmployee(String cf, HashMap<String, Object> m, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException{
		Employee e = updateAtt(cf, m, em);
		return pushUpdated(e, em);
	}
	
	public static boolean updateEmployee(String cf, HashMap<String, Object> m, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException{
		Employee e = updateAtt(cf, m, em);
		e.setSupermarket(supermarket);
		UpdateSupermarketEmployee(e, supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateEmployee(String cf, HashMap<String, Object> m, Employee boss, Supermarket supermarket, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException{
		Employee e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		UpdateSupermarketEmployee(e, supermarket);
		return pushUpdated(e, em);
	}
	
	public static boolean updateEmployee(String cf, HashMap<String, Object> m, Employee boss, Supermarket supermarket, Collection<Product> products, EntityManager em) throws NotFoundException, UpdateException, NotValidCfException{
		Employee e = updateAtt(cf, m, em);
		e.setBoss(boss);
		e.setSupermarket(supermarket);
		UpdateSupermarketEmployee(e, supermarket);
		e.setProducts(products);
		return pushUpdated(e, em);
	}
	
	public static boolean deleteEmployee(String cf, EntityManager em) {
		try {
			EmployeeOperations.delete(cf, em);
		}
		catch(NotFoundException e){
			return false;
		}
		return true;
	}
	
	public static Collection<Employee> searchBySupermarket(Supermarket supermarket, EntityManager em) {
		Query q = em.createQuery("SELECT e FROM Employee e WHERE e.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Employee>) q.getResultList();
	}
	
	public static Collection<Employee> searchBySupermarket(int id, EntityManager em) throws NotFoundException {
		Supermarket supermarket = SupermarketOperations.findOccurence(id, em);
		Query q = em.createQuery("SELECT e FROM Employee e WHERE e.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Employee>) q.getResultList();
	}
	
	public static Collection<Employee> searchByBoss(Employee boss, EntityManager em) {
		Query q = em.createQuery("SELECT e FROM Employee e WHERE e.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Employee>) q.getResultList();
	}
	
	public static Collection<Employee> searchByBoss(String cf, EntityManager em) throws NotFoundException {
		Employee boss = EmployeeOperations.findOccurence(cf, em);
		Query q = em.createQuery("SELECT e FROM Employee e WHERE e.boss = :boss");
		q.setParameter("boss", boss);
		return (Collection<Employee>) q.getResultList();
	}
	
	public static Collection<Employee> searchBySalary(double min, double max, EntityManager em){
		Query q = em.createQuery("SELECT e FROM Employee e");
		Collection<Employee> result = (Collection<Employee>) q.getResultList();
		Collection<Employee> returns = new ArrayList<Employee>();
		result.forEach((temp) -> {
            if(temp.getSalary()< max && temp.getSalary()> min) {
            	returns.add(temp);
            };
		});
		return returns;
	}
	
	
}
