package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Employee;
import entities.Product;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.EmployeeOperations;
import operations.ProductOperations;
import operations.SupermarketOperations;

public class ServiceProduct {
	
	private static Product insertProduct(int id, String name, String description, int weight,
			Collection<Warehouse> warehouses, EntityManager em) throws AlreadyExistException, NotValidException{
		Product p = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(weight <= 0) {
				throw new NotValidException("Weight not valid, insert a valid one");
			}
			p = new Product(id, name, description, weight, warehouses);
			ProductOperations.addNew(p, em);
			if(warehouses != null) {
				UpdateWarehousesProduct(p, warehouses);
			}
		}
		catch(PersistenceException e) { 
			em.detach(p);
			return null;  
		}
		return p;
	}
	
	private static void UpdateWarehousesProduct(Product p, Collection<Warehouse> warehouses) {
		warehouses.forEach((temp) -> {
			Collection<Product> prods = new ArrayList<Product>();
				if(temp.getProducts() != null) {
					prods = temp.getProducts();
				}
				if(!prods.contains(p))
					prods.add(p);
				temp.setProducts(prods);
		});
	}
	
	public static Product insertProduct(int id, String name, int weight, EntityManager em) 
			throws AlreadyExistException, NotValidException{
		return insertProduct(id, name, "", weight,null, em);
		
	}
	
	public static Product insertProduct(int id, String name, String description, int weight, EntityManager em) throws AlreadyExistException, NotValidException{
		return insertProduct(id, name, description, weight, null, em);
		
	}
	
	public static Product insertProduct(int id, String name, int weight,
			Collection<Warehouse> warehouses, EntityManager em) throws AlreadyExistException, NotValidException{
		return insertProduct(id, name, "", weight, warehouses, em);
		
	}
	
	public static Product findProduct(int id, EntityManager em) throws NotFoundException {
		return ProductOperations.findOccurence(id, em);
	}
	
	private static Product updateAttributes(int id, HashMap<String, Object> m, EntityManager em) throws NotFoundException{
		Product p = ProductOperations.findOccurence(id, em);
		if(m == null) return p;
		if(m.isEmpty()) return p;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				p.setName(m.get(key).toString());
			}
			else if("description".equalsIgnoreCase(key)) {
				p.setDescription(m.get(key).toString());
			}
			else if("weight".equalsIgnoreCase(key)) {
				p.setWeight((int)m.get(key));
			}
		}
		return p;
	}
	
	private static boolean pushUpdated(Product p, EntityManager em) {
		try {
			ProductOperations.updateOccurence(p, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}

	public static boolean updateProduct(int id, HashMap<String, Object> m, 
			EntityManager em) throws NotFoundException {
		Product p = updateAttributes(id, m, em);
		return pushUpdated(p, em);
	}

	public static boolean updateProduct(int id, HashMap<String, Object> m, 
			Collection<Warehouse> warehouses, EntityManager em) throws NotFoundException {
		Product p = updateAttributes(id, m, em);
		p.setWarehouses(warehouses);
		UpdateWarehousesProduct(p, warehouses);
		return pushUpdated(p, em);
	}

	public static boolean deleteProduct(int id, EntityManager em) {
		try {
			ProductOperations.delete(id, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}
	
	
	public static Collection<Product> searchByName(String name, EntityManager em){
		Query q = em.createQuery("SELECT p FROM Product p WHERE p.name = :name");
		q.setParameter("name", name);
		return (Collection<Product>) q.getResultList();
	}
	
	public static Collection<Product> searchByWarehouse(Warehouse warehouse, EntityManager em){
		Query q = em.createQuery("SELECT p FROM Product p");
		Collection<Product> result = (Collection<Product>) q.getResultList();
		Collection<Product> returns = new ArrayList<Product>();
		result.forEach((temp) -> {
			Collection<Warehouse> ware = temp.getWarehouses();
			if(ware.contains(warehouse))
				returns.add(temp);
		});
		return returns;
	}
	
	public static Collection<Product> searchByWeight(double min, double max, EntityManager em){
		Query q = em.createQuery("SELECT p FROM Product p");
		Collection<Product> result = (Collection<Product>) q.getResultList();
		Collection<Product> returns = new ArrayList<Product>();
		result.forEach((temp) -> {
            if(temp.getWeight()< max && temp.getWeight()> min){
            	returns.add(temp);
            };
		});
		return returns;
	}
	
}
