package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Employee;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.SupermarketOperations;

public class ServiceSupermarket {
	
	public static Supermarket insertSupermarket(int id, String name, String city, String typology, Collection<Employee> employees, EntityManager em) throws AlreadyExistException, NotValidException, NotFoundException{
		Supermarket s = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(city.equals("")) {
				throw new NotValidException("City field is empty, insert one");
			}
			s = new Supermarket(id, name, city, typology, employees);
			SupermarketOperations.addNew(s, em);
			if(employees != null) {
				UpdateEmployeeSupermarket(s, employees);
			}
		}
		catch(PersistenceException e) { 
			em.detach(s);
			return null;  
		}
		return s;
	}
	
	public static Supermarket insertSupermarket(int id, String name, String city, EntityManager em) throws AlreadyExistException, NotValidException, NotFoundException{
		return insertSupermarket(id, name, city, "",null, em);
		
	}
	
	public static Supermarket insertSupermarket(int id, String name, String city, String typology, EntityManager em) throws AlreadyExistException, NotValidException, NotFoundException{
		return insertSupermarket(id, name, city, typology,null, em);
		
	}
	
	public static Supermarket insertSupermarket(int id, String name, String city, 
			Collection<Employee> employees, EntityManager em) throws AlreadyExistException, NotValidException, NotFoundException{
		return insertSupermarket(id, name, city, "", employees, em);
		
	}
	
	public static Supermarket findSupermarket(int id, EntityManager em) throws NotFoundException {
		return SupermarketOperations.findOccurence(id, em);
	}
	
	private static void UpdateEmployeeSupermarket(Supermarket s, Collection<Employee> employees) {
		employees.forEach((temp) -> {
				temp.setSupermarket(s);
		});
	}
	
	private static Supermarket updateAttributes(int id, HashMap<String, String> m, EntityManager em) throws NotFoundException{
		Supermarket s = SupermarketOperations.findOccurence(id, em);
		if(m == null) return s;
		if(m.isEmpty()) return s;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				s.setName(m.get(key));
			}
			else if("city".equalsIgnoreCase(key)) {
				s.setCity(m.get(key));
			}
			else if("typology".equalsIgnoreCase(key)) {
				s.setTypology(m.get(key));
			}
		}
		return s;
	}
	
	private static boolean pushUpdated(Supermarket s, EntityManager em) {
		try {
			SupermarketOperations.updateOccurence(s, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}

	public static boolean updateSupermarket(int id, HashMap<String, String> m, 
			EntityManager em) throws NotFoundException {
		Supermarket s = updateAttributes(id, m, em);
		return pushUpdated(s, em);
	}

	public static boolean updateSupermarket(int id, HashMap<String, String> m, 
			Collection<Employee> employees, EntityManager em) {
		Supermarket s = null;
		try {
		s = updateAttributes(id, m, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		s.setEmployees(employees);
		UpdateEmployeeSupermarket(s, employees);
		return pushUpdated(s, em);
	}

	public static boolean deleteSupermarket(int id, EntityManager em) {
		try {
			SupermarketOperations.delete(id, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}
	
	public static Collection<Supermarket> searchByCity(String city, EntityManager em){
		Query q = em.createQuery("SELECT s FROM Supermarket s WHERE s.city = :city");
		q.setParameter("city", city);
		return (Collection<Supermarket>) q.getResultList();
	}
	
	public static Collection<Supermarket> searchByTypology(String typology, EntityManager em){
		Query q = em.createQuery("SELECT s FROM Supermarket s WHERE s.typology = :typology");
		q.setParameter("typology", typology);
		return (Collection<Supermarket>) q.getResultList();
	}
	
	public static Collection<Supermarket> searchByEmployee(Employee employee, EntityManager em){
		Query q = em.createQuery("SELECT s FROM Supermarket s");
		Collection<Supermarket> result = (Collection<Supermarket>) q.getResultList();
		Collection<Supermarket> returns = new ArrayList<Supermarket>();
		result.forEach((temp) -> {
			Collection<Employee> emp = temp.getEmployees();
			if(emp.contains(employee))
				returns.add(temp);
		});
		return returns;
	}
	
}
