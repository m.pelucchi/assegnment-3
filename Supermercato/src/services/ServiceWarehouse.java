package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Product;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCfException;
import exceptions.NotValidException;
import operations.WarehouseOperations;

public class ServiceWarehouse {
	
	private static Warehouse insertWarehouse(int id, String name, String city, Supermarket supermarket, 
			Collection<Product> products, EntityManager em) throws AlreadyExistException, NotValidException{
		Warehouse w = null;
		try {
			if(name.equals("")) {
				throw new NotValidException("Name field is empty, insert one");
			}
			if(city.equals("")) {
				throw new NotValidException("City field is empty, insert one");
			}
			w = new Warehouse(id, name, city, supermarket, products);
			WarehouseOperations.addNew(w, em);
			if(products != null) {
				UpdateProductWarehouse(w, products);
			}
		}
		catch(PersistenceException e) { 
			em.detach(w);
			return null;  
		}
		return w;
	}
	
	private static void UpdateProductWarehouse(Warehouse w, Collection<Product> products) {
		products.forEach((temp) -> {
			Collection<Warehouse> wares = new ArrayList<Warehouse>();
				if(temp.getWarehouses() != null) {
					wares = temp.getWarehouses();
				}
				if(!wares.contains(w))
					wares.add(w);
				temp.setWarehouses(wares);
		});
	}
	
	public static Warehouse insertWarehouse(int id, String name, String city, EntityManager em) throws AlreadyExistException, NotValidException{
		return insertWarehouse(id, name, city, null,null, em);
		
	}
	
	public static Warehouse insertWarehouse(int id, String name, String city, Supermarket supermarket, 
			EntityManager em) throws AlreadyExistException, NotValidException{
		return insertWarehouse(id, name, city, supermarket,null, em);
		
	}
	
	public static Warehouse insertWarehouse(int id, String name, String city, 
			Collection<Product> products, EntityManager em) throws AlreadyExistException, NotValidException{
		return insertWarehouse(id, name, city, null, products, em);
		
	}
	
	public static Warehouse findWarehouse(int id, EntityManager em) throws NotFoundException {
		return WarehouseOperations.findOccurence(id, em);
	}
	
	private static Warehouse updateAttributes(int id, HashMap<String, String> m, EntityManager em) throws NotFoundException{
		Warehouse w = WarehouseOperations.findOccurence(id, em);
		if(m == null) return w;
		if(m.isEmpty()) return w;
		Set<String> keys = m.keySet();
		for (String key : keys) {
			if("name".equalsIgnoreCase(key)) {
				w.setName(m.get(key));
			}
			else if("city".equalsIgnoreCase(key)) {
				w.setCity(m.get(key));
			}
		}
		return w;
	}
	
	private static boolean pushUpdated(Warehouse w, EntityManager em) throws NotValidCfException {
		try {
			WarehouseOperations.updateOccurence(w, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}

	public static boolean updateWarehouse(int id, HashMap<String, String> m, 
			EntityManager em) throws NotFoundException, NotValidCfException {
		Warehouse w = updateAttributes(id, m, em);
		return pushUpdated(w, em);
	}

	public static boolean updateWarehouse(int id, HashMap<String, String> m, 
			Collection<Product> products, EntityManager em) throws NotFoundException, NotValidCfException {
		Warehouse w = updateAttributes(id, m, em);
		w.setProducts(products);
		UpdateProductWarehouse(w, products);
		return pushUpdated(w, em);
	}
	
	public static boolean updateWarehouse(int id, HashMap<String, String> m, Supermarket supermarket, 
			EntityManager em) throws NotFoundException, NotValidCfException {
		Warehouse w = updateAttributes(id, m, em);
		w.setSupermarket(supermarket);
		return pushUpdated(w, em);
	}
	
	public static boolean updateWarehouse(int id, HashMap<String, String> m, Supermarket supermarket, 
			Collection<Product> products, EntityManager em) throws NotFoundException, NotValidCfException {
		Warehouse w = updateAttributes(id, m, em);
		w.setSupermarket(supermarket);
		w.setProducts(products);
		UpdateProductWarehouse(w, products);
		return pushUpdated(w, em);
	}

	public static boolean deleteWarehouse(int id, EntityManager em) {
		try {
			WarehouseOperations.delete(id, em);
		}
		catch(NotFoundException e) {
			return false;
		}
		return true;
	}
	
	public static Collection<Warehouse> searchByCity(String city, EntityManager em){
		Query q = em.createQuery("SELECT w FROM Warehouse w WHERE w.city = :city");
		q.setParameter("city", city);
		return (Collection<Warehouse>) q.getResultList();
	}
	
	public static Collection<Warehouse> searchBySupermarket(Supermarket supermarket, EntityManager em){
		Query q = em.createQuery("SELECT w FROM Warehouse w WHERE w.supermarket = :supermarket");
		q.setParameter("supermarket", supermarket);
		return (Collection<Warehouse>) q.getResultList();
	}
	
	public static Collection<Warehouse> searchByProduct(Product product, EntityManager em){
		Query q = em.createQuery("SELECT w FROM Warehouse w");
		Collection<Warehouse> result = (Collection<Warehouse>) q.getResultList();
		Collection<Warehouse> returns = new ArrayList<Warehouse>();
		result.forEach((temp) -> {
			Collection<Product> prod = temp.getProducts();
			if(prod.contains(product))
				returns.add(temp);
		});
		return returns;
	}
	
}
