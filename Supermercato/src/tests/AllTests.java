package tests;

import org.junit.runner.RunWith;



import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	SupermarketTest.class, WarehouseTest.class,
	CashierTest.class, ProductTest.class, 
	ButcherTest.class,EmployeeTest.class
})
public class AllTests {

}