package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Butcher;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import operations.ButcherOperations;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ButcherTest {
		
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Product> products;
	private static Collection<Butcher> butchers;

	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		products = new ArrayList <Product>();
		butchers = new ArrayList <Butcher>();
		System.out.println(" ");
		System.out.println("Start Butcher operation test");
	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {
		Butcher b = null;
		Butcher b2 = null;
		
		try {
			b = new Butcher("RMNLCU96B12I577Y", "Luca", "Romanato" , 12, 2, 1996,  20, 800, null, null, null, "Salumiere");
			b2 = new Butcher("CRLPVT76M08C540U","Carcarlo", "Pravettoni", 8, 8, 1976, 35, 1000, null , null ,null , "affettatore");

			butchers.add(b);
			butchers.add(b2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    ButcherOperations.addNew(b, entitymanager);
	    ButcherOperations.addNew(b2, entitymanager);
	    
	    Butcher search = null;
	    try {
			search = ButcherOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(b.equals(search));
	}
	
	@Test
	public void test2_UpdateOccurrenceNoRelations() {
	    Butcher b = null;
	    int newSchedule = 1200;
	    try{
	    	b = ButcherOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    b.setSchedule(newSchedule);
	    try{
	    	ButcherOperations.updateOccurence(b, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    Butcher search = null;
	    try {
			search = ButcherOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    if(search.getSchedule() == newSchedule) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}
	
	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	ButcherOperations.delete("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	   
	    try {
	    	ButcherOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(NotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }finally {
	    	
	    }   
	}    
	
	@Test
	public void test4_SuccesfullAddOccurrenceRelation_Employee() throws AlreadyExistException {
		Butcher b = null;
		Butcher e1 = null;
		Supermarket s = null;
		Product p = null;
		Product p2 = null;
		
		try {
			e1 = ButcherOperations.findOccurence("CRLPVT76M08C540U", entitymanager);
			
			p = new Product(1,"Panettone", "Panettone Motta" , 200 , null);
			p2 = new Product(2,"Datteri", "Datteri del Madagascar" , 70 , null);
			products.add(p);
			products.add(p2);
			s = new Supermarket(3, "Esselunga", "Cernusco", "Iper-Store", null);
			b = new Butcher ("LDADIO88R08B709H", "Aldo", "Ido", 8, 10, 1988, 48, 1380, e1, s, products, "salumiere");
		
		    butchers.add(b);
		}
		    catch(Exception ex) {
				fail(ex.getMessage());
			}
		    ButcherOperations.addNew(b, entitymanager);
		    
		    
		    Butcher search = null;
		    try {
				search = ButcherOperations.findOccurence("LDADIO88R08B709H", entitymanager);
			}
			catch(Exception ex) {
				fail(ex.getMessage());
			}
		    if((search.getBoss().equals(e1))&&(search.getSupermarket().equals(s))) {
		    	//assert(true);
		    }else {
		    	//assert(false);
		    }
		   try {
		    	ButcherOperations.delete("LDADIO88R08B709H", entitymanager);
		    }
		    catch(Exception ex) {
		    	fail(ex.getMessage());
		    }
		   
		    try {
		    	
		    	ButcherOperations.findOccurence("LDADIO88R08B709H", entitymanager);
		    }
		    catch(NotFoundException ex) {
		    	return;
		    }
		    catch(Exception ex) {
		    	fail(ex.getMessage());
		    }finally {
		    	
		    }  
		    
		    butchers.clear();
		    products.clear();
		}
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Butcher butcher : butchers) {
			try{
				ButcherOperations.delete(butcher.getCf(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
}

