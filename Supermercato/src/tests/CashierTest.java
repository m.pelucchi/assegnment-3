package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Cashier;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidCashNumberException;
import operations.CashierOperations;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CashierTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Product> products;
	private static Collection<Cashier> cashiers;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		products = new ArrayList <Product>();
		cashiers = new ArrayList <Cashier>();
		System.out.println(" ");
		System.out.println("Start Cashier operation test");
	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {
		Cashier c = null;
		Cashier c1 = null;
		try {
			c = new Cashier("RMNLCU96B12I577Y", "Luca", "Romanato" , 12, 3, 1995,  20, 800, null, null, null, 1);
			c1 = new Cashier("CRLPVT76M08C540U","Carcarlo", "Pravettoni", 8, 8, 1976, 35, 1000, null , null ,null ,2);
			cashiers.add(c);
			cashiers.add(c1);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    CashierOperations.addNew(c, entitymanager);
	    CashierOperations.addNew(c1, entitymanager);
	    
	    Cashier search = null;
	    try {
			search = CashierOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(c.equals(search));
	}
	
	@Test
	public void test2_UpdateOccurrenceNoRelations() throws NotValidCashNumberException {
	    Cashier c = null;
	    int newCashNumber = 3;
	    try{
	    	c = CashierOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    c.setCashNumber(newCashNumber);
	    try{
	    	CashierOperations.updateOccurence(c, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    Cashier search = null;
	    try {
			search = CashierOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    if(search.getCashNumber() == newCashNumber) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}
	
	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	CashierOperations.delete("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	   
	    try {
	    	CashierOperations.findOccurence("RMNLCU96B12I577Y", entitymanager);
	    }
	    catch(NotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }finally {
	    	
	    }   
	} 
	@Test
	public void test4_SuccesfullAddOccurrenceRelation_Employee() throws AlreadyExistException {
		Cashier c2 = null;
		Cashier c1 = null;
		Supermarket s = null;
		Product p = null;
		Product p2 = null;
		
		try {
			c1 = CashierOperations.findOccurence("CRLPVT76M08C540U", entitymanager);
			p = new Product(1,"Panettone", "Panettone Motta" , 200 , null);
			p2 = new Product(2,"Datteri", "Datteri del Madagascar" , 70 , null);
			products.add(p);
			products.add(p2);
			s = new Supermarket(3, "Esselunga", "Cernusco", "Iper-Store", null);
			c2 = new Cashier("MNDPLE94A01L682W","Amanda", "Pela", 1, 1, 1994, 35, 1000, c1 , s , products, 7);
			cashiers.add(c2);
		}
		    catch(Exception ex) {
				fail(ex.getMessage());
			}
		    CashierOperations.addNew(c2, entitymanager);
		   
		    Cashier search = null;
		    try {
				search = CashierOperations.findOccurence("MNDPLE94A01L682W", entitymanager);
			}
			catch(Exception ex) {
				fail(ex.getMessage());
			}
		    if((search.getBoss().equals(c1))&&(search.getSupermarket().equals(s))) {
		    	//assert(true);
		    }else {
		    	//assert(false);
		    }
		    try {
		    	CashierOperations.delete("MNDPLE94A01L682W", entitymanager);
		    }
		    catch(Exception ex) {
		    	fail(ex.getMessage());
		    }
		   
		    try {
		    	
		    	CashierOperations.findOccurence("MNDPLE94A01L682W", entitymanager);
		    }
		    catch(NotFoundException ex) {
		    	return;
		    }
		    catch(Exception ex) {
		    	fail(ex.getMessage());
		    }finally {
		    	
		    }  
		    
		    cashiers.clear();
		    products.clear();
		}
	
	@Test
	public void test5_SetWrongCashNumber()  throws NotValidCashNumberException { 
		Cashier e = null;
		try {
			e = new Cashier("LCCSMN80A01E063U","Luca", "Rizzi", 1, 1, 2000, 42, 1850, null, null, null, -4);
			cashiers.add(e);
			}
		catch(Exception ex) {
			//assert(true);
			fail(ex.getMessage());
		}
	   
	}
	
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Cashier cashier : cashiers) {
			try{
				CashierOperations.delete(cashier.getCf(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
}