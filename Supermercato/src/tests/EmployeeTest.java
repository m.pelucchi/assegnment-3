package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Employee;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import operations.EmployeeOperations;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Employee> employees;
	private static Collection<Product> products;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		employees = new ArrayList <Employee>();
		products = new ArrayList <Product>();
		new ArrayList <Supermarket>();
		System.out.println(" ");
		System.out.println("Start Employee operation test");
		
	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {
		Employee e = null;
		Employee e1 = null;
		try {
			e = new Employee("LCCSMN80A01E063U","Luca", "Rizzi", 1, 1, 1980, 42, 1850, null, null, null);
			e1 = new Employee("MTTPCC80A01E715Y","Franco", "Bello", 1, 1, 1980, 48, 1970, null,null,null);
		    employees.add(e);
		    employees.add(e1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    EmployeeOperations.addNew(e, entitymanager);
	    EmployeeOperations.addNew(e1, entitymanager);
	    
	    Employee search = null;
	    Employee search1 = null;
	    try {
			search = EmployeeOperations.findOccurence("LCCSMN80A01E063U", entitymanager);
			search1 = EmployeeOperations.findOccurence("MTTPCC80A01E715Y", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    assert(e.equals(search));
	    assert(e1.equals(search1));
	}
	
	@Test
	public void test2_UpdateOccurrenceNoRelations() {
	    Employee e = null;
	    Double newSalary = 1700.00;
	    try{
	    	e = EmployeeOperations.findOccurence("MTTPCC80A01E715Y", entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	    e.setSalary(newSalary);
	    try{
	    	EmployeeOperations.updateOccurence(e, entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	    
	    Employee search = null;
	    try {
			search = EmployeeOperations.findOccurence("MTTPCC80A01E715Y", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    if(search.getSalary() == newSalary) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}
	
	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	EmployeeOperations.delete("LCCSMN80A01E063U", entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	   
	    try {
	    	EmployeeOperations.findOccurence("LCCSMN80A01E063U", entitymanager);
	    }
	    catch(NotFoundException ex) {
	    	return;
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }finally {
	    	
	    }   
	}    
	

	@Test
	public void test4_SuccesfullAddOccurrenceRelation_Employee() throws AlreadyExistException {
		Employee e = null;
		Employee e1 = null;
		Supermarket s = null;
		Product p = null;
		Product p2 = null;
		
		try {
			e1 = EmployeeOperations.findOccurence("MTTPCC80A01E715Y", entitymanager);
			
			p = new Product(1,"Panettone", "Panettone Motta" , 200 , null);
			p2 = new Product(2,"Datteri", "Datteri del Madagascar" , 70 , null);
			products.add(p);
			products.add(p2);
			
			e = new Employee("TMSCMN80A01E715W", "Alessandro", "Giovinazzi", 1, 1, 1973, 38, 1080, e1, null, products);
			
		    employees.add(e);
		    s = new Supermarket(3, "Esselunga", "Cernusco", "Iper-Store", null);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    EmployeeOperations.addNew(e, entitymanager);
	    
	    e.setSupermarket(s);
	    try{
	    	EmployeeOperations.updateOccurence(e, entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	    Employee search = null;
	    try {
			search = EmployeeOperations.findOccurence("TMSCMN80A01E715W", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    if((search.getBoss().equals(e1))&&(search.getSupermarket().equals(s))) {
	    	//assert(true);
	    }else {
	    	//assert(false);
	    }
	    try {
	    	EmployeeOperations.delete("TMSCMN80A01E715W", entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	   
	    try {
	    	
	    	EmployeeOperations.findOccurence("TMSCMN80A01E715W", entitymanager);
	    }
	    catch(NotFoundException ex) {
	    	return;
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }finally {
	    	
	    }   
	    
	    employees.clear();
	    products.clear();
	}	
	@Test
	public void test5_GetIdDuplicate() throws AlreadyExistException { 
		Employee e = null;
		Employee e1 = null;
		try {
			e = new Employee("LCCSMN80A01E063U","Luca", "Rizzi", 1, 1, 2000, 42, 1850, null, null, null);
			e1 = new Employee("LCCSMN80A01E063U","Luca", "Rizzi", 1, 1, 1960, 48, 1970, null,null,null);
		    employees.add(e);
		    employees.add(e1);
		}
		catch(Exception ex) {
			assert(true);
		}
	}
	
	@Test
	public void test6_SearchItemInexistent() throws NotFoundException { 
		Employee e = null;
	    try{
	    	e = EmployeeOperations.findOccurence("LTCSMN80A01E077U", entitymanager);
	    }
	    catch(Exception ex) {
	    	assert(true);
	    }
	}
	
	
	@Test
	public void test7_SetWrongCF() throws AlreadyExistException { 
		Employee e = null;
		try {
			e = new Employee("TTTSMN80A01E063U","Luca", "Rizzi", 1, 1, 2000, 42, 1850, null, null, null);
			employees.add(e);
			}
		catch(Exception ex) {
			assert(true);
		}
	   
	}
	
	@Test
	public void test8_SetWrongDate() throws AlreadyExistException { 
		Employee e = null;
		try {
			e = new Employee("LCCSMN80A01E063U","Luca", "Rizzi", -1, 1, 2000, 42, 1850, null, null, null);
			employees.add(e);
			}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	   
	}

	@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Employee employee : employees) {
			try{
				EmployeeOperations.delete(employee.getCf(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
}