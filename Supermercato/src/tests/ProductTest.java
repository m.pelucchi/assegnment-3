package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Product;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import operations.ProductOperations;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Warehouse> warehouses; 
	private static Collection<Product> products;

	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		warehouses = new ArrayList <Warehouse>();
		products = new ArrayList <Product>();
		System.out.println(" ");
		System.out.println("Start Product operation test");

	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {
		Product p = null;
		Product p2 = null;
		try {
			p = new Product(1,"Panettone", "Panettone Motta" , 200 , null);
			p2 = new Product(3,"Datteri", "Datteri del Madagascar" , 70 , null);
			products.add(p);
			products.add(p2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		ProductOperations.addNew(p, entitymanager);
		ProductOperations.addNew(p2, entitymanager);
	    
		Product search = null;
	    try {
			search = ProductOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(p.equals(search));
	}
	
	@Test
	public void test2_UpdateOccurrenceNoRelations() {
		Product p = null;
	    int newWeight= 80;
	    
	    try{
	    	p = ProductOperations.findOccurence(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    p.setWeight(newWeight);
	    try{
	    	ProductOperations.updateOccurence(p, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    Product search = null;
	    try {
			search = ProductOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    if(search.getWeight() == newWeight) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}
	
	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	ProductOperations.delete(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	   
	    try {
	    	ProductOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }finally {
	    	
	    }   
	}    
	@Test
	public void test4_SuccesfullAddOccurrenceRelation_Product() throws AlreadyExistException {
		Warehouse w = null;
		Warehouse w2 = null;
		Product p = null;
		Product p2 = null;
		Product p4 = null;
		Supermarket s = null;
		
		try {
			s = new Supermarket(3, "Esselunga", "Cernusco", "Iper-Store", null);
			w = new Warehouse(1, "MagazzinoAnimali", "Bernareggio", s, null);
			w2 = new Warehouse(2, "MagazzinoFrutta", "Bernareggio", s, null);
			warehouses.add(w);
			warehouses.add(w2);
			p = new Product(1, "Manzo", "Puro manzo italiano", 200, warehouses);
			p2 = new Product(2, "Avocado", "Avocado hass", 60, warehouses);
			p4 = new Product(4, "Fagioli", "Fagioli neri", 160, warehouses);
		    products.add(p);
		    products.add(p2);
		    products.add(p4);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    ProductOperations.addNew(p, entitymanager);
	    ProductOperations.addNew(p2, entitymanager);
	    ProductOperations.addNew(p4, entitymanager);
	    
	    Product search = null;
	    try {
			search = ProductOperations.findOccurence(1, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    if((search.getId()==1)&&(search.getName().equals("Manzo"))) {
	    	//assert(true);
	    }else {
	    	//assert(false);
	    }
	    try {
	    	ProductOperations.delete(1, entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	   
	    try {
	    	
	    	ProductOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException ex) {
	    	return;
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }finally {
	    	
	    }   
	    warehouses.clear();
	    products.clear();
	}
	
	@Test
	public void test5_GetIdDuplicate() throws AlreadyExistException { 
		Product p = null;
		Product p2 = null;
		try {
			p = new Product(4,"Pandoro", "Pandoro Melegatti" , 200 , null);
			p2 = new Product(4,"Guanti", "Guanti da sci" , 70 , null);
			products.add(p);
			products.add(p2);
		}
		catch(Exception e) {
			assert(true);
		}
	}
	
	@Test
	public void test6_SearchItemInexistent() throws NotFoundException { 
		Product p = null;
	    try{
	    	p = ProductOperations.findOccurence(7, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	}
	
	@Test
	public void test7_SetIdDuplicate() throws AlreadyExistException { 
		Product p = null;
		Product p2 = null;
		try{
	    	p = new Product(4,"Pandoro", "Pandoro Melegatti" , 200 , null);
	    	p2 = new Product(5,"Guanti", "Guanti da sci" , 70 , null);
	    	products.add(p);
	    	products.add(p2);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    p.setId(5);
	    try{
	    	ProductOperations.updateOccurence(p, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	}
	
	
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Product product : products) {
			try{
				ProductOperations.delete(product.getId(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
}