package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Employee;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import operations.SupermarketOperations;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SupermarketTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Supermarket> supermarkets;
	private static Collection<Employee> employees;


	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		supermarkets = new ArrayList <Supermarket>();
		employees = new ArrayList <Employee>();
		System.out.println(" ");
		System.out.println("Start Supermarket operation test");

	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {
		Supermarket s = null;
		Supermarket s1 = null;
		try {
			s = new Supermarket(1,"Famila", "Milano", "Super-store", null);
			s1 = new Supermarket(2,"Ringo", "Bergamo", "Iper-store", null);
		    supermarkets.add(s);
		    supermarkets.add(s1);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    SupermarketOperations.addNew(s, entitymanager);
	    SupermarketOperations.addNew(s1, entitymanager);
	    
	    Supermarket search = null;
	    Supermarket search1 = null;
	    try {
			search = SupermarketOperations.findOccurence(1, entitymanager);
			search1 = SupermarketOperations.findOccurence(2, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(s.equals(search));
	    assert(s1.equals(search1));
	}
	
	@Test
	public void test2_UpdateOccurrenceNoRelations() {
	    Supermarket s = null;
	    String newTypology = "Iper-store";
	    try{
	    	s = SupermarketOperations.findOccurence(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    s.setTypology(newTypology);
	    try{
	    	SupermarketOperations.updateOccurence(s, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    Supermarket search = null;
	    try {
			search = SupermarketOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    if(search.getTypology() == newTypology) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}
	
	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	SupermarketOperations.delete(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	   
	    try {
	    	SupermarketOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }finally {
	    	
	    }   
	}    
	
	@Test
	public void test4_Succesfull_AddDelete_OccurrenceRelation_Supermarket() throws AlreadyExistException {
		Employee e2 = null;
		Employee e1 = null;
		Supermarket s = null;
		
		try {
			e2 = new Employee("LCCSMN80A01E063U","Luca", "Rizzi", 1, 1, 1980, 42, 1850, null, null, null);
			e1 = new Employee("MTTPCC80A01E715Y","Franco", "Bello", 1, 1, 1980, 48, 1970, null,null,null);
		    employees.add(e2);
		    employees.add(e1);
			s = new Supermarket(1, "REX", "Calco", "Iper-Store", employees);
			
		    supermarkets.add(s);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    SupermarketOperations.addNew(s, entitymanager);
	    
	    Supermarket search = null;
	    try {
			search = SupermarketOperations.findOccurence(1, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
	    if((search.getId()==1)&&(search.getName().equals("REX"))) {
	    	//assert(true);
	    }else {
	    	//assert(false);
	    }
	    try {
	    	SupermarketOperations.delete(1, entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	   
	    try {
	    	
	    	SupermarketOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException ex) {
	    	return;
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }finally {
	    	
	    }   
	    employees.clear();
	}
	
	@Test
	public void test5_GetIdDuplicate() throws AlreadyExistException { 
		Supermarket s = null;
		Supermarket s1 = null;
		try {
			s = new Supermarket(1,"REX", "Mantova", "Super-store", null);
			s1 = new Supermarket(1,"Auchan", "Melzo", "Iper-store", null);
		    supermarkets.add(s);
		    supermarkets.add(s1);
		}
		catch(Exception e) {
			assert(true);
		}
	    
	} 
	
	@Test
	public void test6_SearchItemInexistent() throws NotFoundException { 
		Supermarket s = null;
	    try{
	    	s = SupermarketOperations.findOccurence(7, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	}
	
	@Test
	public void test7_SetIdDuplicate() throws AlreadyExistException { 
		Supermarket s = null;
		Supermarket s1 = null;
	    try{
	    	s = new Supermarket(5,"Coop", "Matera", "Iper-store", null);
	    	s1 = new Supermarket(4,"Auchan", "Melzo", "Iper-store", null);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    s.setId(4);
	    try{
	    	SupermarketOperations.updateOccurence(s, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	    
	}
	

@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Supermarket supermarket : supermarkets) {
			try{
				SupermarketOperations.delete(supermarket.getId(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
}
