package tests;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Product;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import operations.WarehouseOperations;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WarehouseTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Warehouse> warehouses;
	private static Collection<Product> products;

	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		warehouses = new ArrayList <Warehouse>();
		products = new ArrayList <Product>();
		System.out.println(" ");
		System.out.println("Start Warehouse operation test");
		
	}
	
	@Test
	public void test1_AddAndFindTupleNoRelations() throws AlreadyExistException, NotFoundException {

		Warehouse w = null;
		try {
		    w = new Warehouse(1, "MagazzinoFiori", "Pioltello", null , null);
		    warehouses.add(w);
		    
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    WarehouseOperations.addNew(w, entitymanager);
	    
	    
	    Warehouse search = null;
	    try {
			search = WarehouseOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(w.equals(search));
	}

	
	@Test
	public void test2_UpdateOccurrenceNoRelations() {
		Warehouse w = null;
	    String newCity = "Varese";
	    try{
	    	w = WarehouseOperations.findOccurence(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    w.setCity(newCity);
	    try{
	    	WarehouseOperations.updateOccurence(w, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		    
	   Warehouse search = null;
	    try {
			search = WarehouseOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    if(search.getCity() == newCity) {
	    	assert(true);
	    }else {
	    	assert(false);
	    }
	}

	@Test
	public void test3_DeleteOccurrenceNoRelations() {
	    try {
	    	WarehouseOperations.delete(1, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	   
	    try {
	    	WarehouseOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }finally {
	    	
	    }   

	}    
	
	@Test
	public void test4_Succesfull_AddDelete_OccurrenceRelation__Warehouses() throws AlreadyExistException {
		Warehouse w = null;
		Product p = null;
		Product p2 = null;
		Supermarket s = null;
		
		
		try {
			 p = new Product(1,"Panettone", "Panettone Motta" , 200 , null);
			 p2 = new Product(2,"Datteri", "Datteri del Madagascar" , 70 , null);
			 products.add(p);
			 products.add(p2);
			s = new Supermarket(3, "Esselunga", "Cernusco", "Iper-Store", null);
		   
			w = new Warehouse(1, "MagazzinoAnimali", "Bernareggio", s, null);
			warehouses.add(w);
			WarehouseOperations.addNew(w, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		
		p.setWarehouses(warehouses);
		p2.setWarehouses(warehouses);
		
	    try{
	    	WarehouseOperations.updateOccurence(w, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    
		try {
	    	WarehouseOperations.delete(1, entitymanager);
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }
	   
	    try {
	    	
	    	WarehouseOperations.findOccurence(1, entitymanager);
	    }
	    catch(NotFoundException ex) {
	    	return;
	    }
	    catch(Exception ex) {
	    	fail(ex.getMessage());
	    }finally {
	    	
	    }   
	    warehouses.clear();
	    products.clear();
	}
	
	@Test
	public void test5_GetIdDuplicate() throws AlreadyExistException { 
		Warehouse w = null;
		Warehouse w1 = null;
		try {
			w = new Warehouse(4, "MagazzinoFalegnameria", "Bernareggio", null, null);
			w1 = new Warehouse(4, "MagazzinoCeramiche", "Varese", null, null);
		    warehouses.add(w);
		    warehouses.add(w1);
		}
		catch(Exception e) {
			assert(true);
		}
		
	} 
	
	@Test
	public void test6_SearchItemInexistent() throws NotFoundException { 
		Warehouse w = null;
	    try{
	    	w= WarehouseOperations.findOccurence(7, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	}
	
	@Test
	public void test7_SetIdDuplicate() throws AlreadyExistException { 
		Warehouse w = null;
		Warehouse w1 = null;
	    try{
	    	w = new Warehouse(1, "MagazzinoFalegnameria", "Bernareggio", null, null);
	    	w1 = new Warehouse(4, "MagazzinoCeramiche", "Varese", null, null);
	    	warehouses.add(w);
		    warehouses.add(w1);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    w.setId(4);
	    try{
	    	WarehouseOperations.updateOccurence(w, entitymanager);
	    }
	    catch(Exception e) {
	    	assert(true);
	    }
	    warehouses.clear();
	}


	@AfterClass
	public static void cleanUpDbCloseConnection() {
		for (Warehouse warehouse : warehouses) {
			try{
				WarehouseOperations.delete(warehouse.getId(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
}