package testservice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ButcherServiceTest.class, CashierServiceTest.class, EmployeeServiceTest.class, ProductServiceTest.class,
		SupermarketServiceTest.class, WarehouseServiceTest.class })
public class AllServiceTests {

}
