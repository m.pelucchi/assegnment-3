package testservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Employee;
import entities.Product;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.EmployeeOperations;
import services.ServiceEmployee;
import services.ServiceProduct;
import services.ServiceSupermarket;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeServiceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Employee> employee;

	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		employee = new ArrayList <Employee>();
		System.out.println(" ");
		System.out.println("Start Employee service test");
	}
		
	@Test
	public void  test01_InsertEmployeeOnlyNameAndSurnameAndBDay() {
		Employee e = null;
		try {
			e = ServiceEmployee.insertEmployee("PLZMTN80A41D286B", "Martina", "Pelo", 1, 1, 1980, entitymanager);
			employee.add(e);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(e == null) fail();
		Employee found = null;
		try {
			e = EmployeeOperations.findOccurence("PLZMTN80A41D286B", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceEmployee.findEmployee("PLZMTN80A41D286B", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getCf() == "PLZMTN80A41D286B");
		assert(e.equals(found));
	}
	
	@Test
	public void  test02_InsertEmployeeOnlyNameAndSurnameAndBDayAndScheduleAndSalary() {
		Employee e = null;
		try {
			e = ServiceEmployee.insertEmployee("ZLOGFR80T01B354G", "Zola", "Gianfranco", 1, 12, 1980, 48, 1450, entitymanager);
			employee.add(e);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(e == null) fail();
		Employee found = null;
		try {
			e = EmployeeOperations.findOccurence("ZLOGFR80T01B354G", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceEmployee.findEmployee("ZLOGFR80T01B354G", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getCf() == "ZLOGFR80T01B354G");
		assert(found.getSalary() == 1450);
		assert(found.getSchedule() == 48);
		assert(e.equals(found));
	}
	
	@Test
	public void  test03_InsertEmployeeOnlyNameAndSurnameAndBDayAndScheduleAndSalaryAndSupermarket() {
		Employee e = null;
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(1, "Lidl", "Piacenza", "Iperstore", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			e = ServiceEmployee.insertEmployee("CSSDRD80H15C933C", "Edoardo", "Cassina", 15, 6, 1980, 45, 1630, s.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(e == null) fail();
		Employee found = null;
		try {
			found = ServiceEmployee.findEmployee("CSSDRD80H15C933C", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getSupermarket() == s);
		assert(e.equals(found));
	}
	
	@Test
	public void  test04_InsertEmployeeOnlyNameAndSurnameAndBDayAndScheduleAndSalaryAndSupermarketAndBoss() {
		Employee e = null;
		Employee b = null;
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "U2", "Bologna", "Iperstore", entitymanager);
			b = ServiceEmployee.insertEmployee("BRGFBA80A01C933M", "Fabio", "Bergo", 1, 12, 1980, 45, 1630, s.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			e = ServiceEmployee.insertEmployee("BDAPPP80A01A662F", "Pippo", "Baudo", 10, 11, 1996, 42, 1500, b.getCf(), s.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(e == null) fail();
		Employee found = null;
		try {
			found = ServiceEmployee.findEmployee("BDAPPP80A01A662F", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getBoss() == b);
		assert(e.equals(found));
	}
	
	@Test
	public void  test05_InsertEmployeeOnlyNameAndSurnameAndBDayAndScheduleAndSalaryAndSupermarketAndProducts() {
		Employee e = null;
		Product p = null;
		Product p1 = null;
		int[] productId = new int [2];
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(3, "Iperal", "Bologna", "Iperstore", entitymanager);
			p = ServiceProduct.insertProduct(1, "Mele", "Golden", 10, entitymanager);
			productId[0] = p.getId();
			p1 = ServiceProduct.insertProduct(2, "Mele", "Renetta", 15, entitymanager);
			productId[1] = p1.getId();
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			e = ServiceEmployee.insertEmployee("PLZMTN98R23D286X", "Martino", "Pelizzo", 23, 10, 1998, 42, 1500, productId, s.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(e == null) fail();
		Employee found = null;
		try {
			found = ServiceEmployee.findEmployee("PLZMTN98R23D286X", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getProducts().contains(p));
		assert(found.getProducts().contains(p1));
		assert(e.equals(found));
	}
	
	@Test
	public void test06_SuccesfullUpdateNoRelations() {
		Calendar date = Calendar.getInstance();
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Marco");
		m.put("surname", "Guido");
		date.set(Calendar.DAY_OF_MONTH, 22);
		date.set(Calendar.MONTH, 9);
		date.set(Calendar.YEAR, 1987);
		m.put("date", date);
		m.put("schedule", 35);
		m.put("salary", 1600);
		
		try {
			assert(ServiceEmployee.updateEmployee("ZLOGFR80T01B354G", m, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Employee e = null;
		try {
			e = ServiceEmployee.findEmployee("ZLOGFR80T01B354G", entitymanager);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
		assert(e.getName() == "Marco");
		assert(e.getSurname() == "Guido");
		assert(e.getDate() == date);
		assert(e.getSchedule() == 35);
		assert(e.getSalary() == 1600);
	}
	
	@Test
	public void test07_SuccesfullUpdateWithSupermarket() {
		Calendar date = Calendar.getInstance();
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Martina");
		m.put("surname", "Maldita");
		date.set(Calendar.DAY_OF_MONTH, 26);
		date.set(Calendar.MONTH, 10);
		date.set(Calendar.YEAR, 1998);
		m.put("date", date);
		m.put("schedule", 36);
		m.put("salary", 1670);
		Supermarket supers = null;
		try {
			supers =  ServiceSupermarket.insertSupermarket(4, "Famila", "Seveso", "Store", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceEmployee.updateEmployee("PLZMTN80A41D286B", m, supers, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Employee e = null;
		try {
			e = ServiceEmployee.findEmployee("PLZMTN80A41D286B", entitymanager);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
		assert(e.getName() == "Martina");
		assert(e.getSurname() == "Maldita");
		assert(e.getDate() == date);
		assert(e.getSchedule() == 36);
		assert(e.getSalary() == 1670);
		assert(e.getSupermarket() == supers);
	}
		
	@Test
	public void test08_SuccesfullUpdateWithSupermarketAndBoss() {
		Calendar date = Calendar.getInstance();
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Martina");
		m.put("surname", "Maldita");
		date.set(Calendar.DAY_OF_MONTH, 26);
		date.set(Calendar.MONTH, 10);
		date.set(Calendar.YEAR, 1998);
		m.put("date", date);
		m.put("schedule", 36);
		m.put("salary", 1670);
		Supermarket supers = null;
		Employee boss = null;
		try {
			supers =  ServiceSupermarket.insertSupermarket(5, "Famila", "Limbiate", "Store", entitymanager);
			boss = ServiceEmployee.insertEmployee("RMOLCU80A01E715S", "Luca", "Roma", 10, 10, 1870, 35, 1345, supers.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceEmployee.updateEmployee("PLZMTN80A41D286B", m, boss, supers, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Employee e = null;
		try {
			e = ServiceEmployee.findEmployee("PLZMTN80A41D286B", entitymanager);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
		assert(e.getName() == "Martina");
		assert(e.getSurname() == "Maldita");
		assert(e.getDate() == date);
		assert(e.getSchedule() == 36);
		assert(e.getSalary() == 1670);
		assert(e.getSupermarket() == supers);
		assert(e.getBoss() == boss);
	}
	
	@Test
	public void test09_SuccesfullUpdateWithSupermarketAndBoss() {
		Calendar date = Calendar.getInstance();
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Edoardo");
		m.put("surname", "Cascione");
		date.set(Calendar.DAY_OF_MONTH, 26);
		date.set(Calendar.MONTH, 10);
		date.set(Calendar.YEAR, 1998);
		m.put("date", date);
		m.put("schedule", 36);
		m.put("salary", 1670);
		Supermarket supers = null;
		Employee boss = null;
		Product p = null;
		Product p1 = null;
		Product p2 = null;
		Collection<Product> prods = new ArrayList<Product>();
		try {
			p = ServiceProduct.insertProduct(3, "Fragole", 10, entitymanager);
			prods.add(p);
			p1 = ServiceProduct.insertProduct(4, "Banane", 15, entitymanager);
			prods.add(p1);
			p2 = ServiceProduct.insertProduct(5, "Albicocche", 1, entitymanager);
			supers =  ServiceSupermarket.insertSupermarket(6, "Billa", "Seregno", "Store", entitymanager);
			boss = ServiceEmployee.insertEmployee("BRNCRD80A01F205P", "Corrado", "Berna", 10, 10, 1870, 35, 1345, supers.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceEmployee.updateEmployee("CSSDRD80H15C933C", m, boss, supers, prods, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Employee e = null;
		try {
			e = ServiceEmployee.findEmployee("CSSDRD80H15C933C", entitymanager);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
		assert(e.getName() == "Edoardo");
		assert(e.getSurname() == "Cascione");
		assert(e.getDate() == date);
		assert(e.getSchedule() == 36);
		assert(e.getSalary() == 1670);
		assert(e.getSupermarket() == supers);
		assert(e.getBoss() == boss);
		assert(e.getProducts().contains(p));
		assert(e.getProducts().contains(p1));
		assertFalse(e.getProducts().contains(p2));
	}
	
	@Test
	public void test10_SuccesfullDeleteEmployee() {
		assert(ServiceEmployee.deleteEmployee("PLZMTN80A41D286B", entitymanager));
		try {
			ServiceEmployee.findEmployee("PLZMTN80A41D286B", entitymanager);
		}
		catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void  test11_SearchEmployeeBySupermarket() {
		Employee e = null;
		Employee e1 = null;
		Employee e2 = null;
		Supermarket s = null;
		Supermarket s1 = null;
		Collection<Employee> result = new ArrayList<Employee>();
		Collection<Employee> result1 = new ArrayList<Employee>();
		try {
			s = ServiceSupermarket.insertSupermarket(7, "Emmelunga", "Lucca", entitymanager);
			s1 = ServiceSupermarket.insertSupermarket(8, "Crai", "Lucca", entitymanager);
			
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			e = ServiceEmployee.insertEmployee("STNLCU80A01C933K", "Stanco", "Luca", 7, 12, 1970, 35, 1345, s.getId(), entitymanager);
			e1 = ServiceEmployee.insertEmployee("PSNLCU80A01D286Y", "Lucia", "Pisanello", 5, 10, 1990, 35, 1345, s.getId(), entitymanager);
			e2 = ServiceEmployee.insertEmployee("LCUFRC80A01A326D", "Federico", "Lucia", 10, 6, 1991, 35, 1345, s1.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceEmployee.searchBySupermarket(s, entitymanager);
			result1 = ServiceEmployee.searchBySupermarket(s, entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(e)); 
		assert(result.contains(e1)); 
		assertFalse(result.contains(e2));
		assert(result1.contains(e)); 
		assert(result1.contains(e1)); 
		assertFalse(result1.contains(e2)); 
	}
	
	@Test
	public void  test12_SearchEmployeeByBoss() {
		Employee e = null;
		Employee e1 = null;
		Employee e2 = null;
		Employee boss = null;
		Employee boss1 = null;
		Collection<Employee> result = new ArrayList<Employee>();
		Collection<Employee> result1 = new ArrayList<Employee>();
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(9, "Esselunga", "Como", entitymanager);
			boss = ServiceEmployee.insertEmployee("MRNFRN80A01D286C", "Floriano", "Mirani", 7, 12, 1970, 35, 1345, s.getId(), entitymanager);
			boss1 = ServiceEmployee.insertEmployee("MRNSMN80A41D286D", "Simona", "Mirani", 5, 10, 1990, 35, 1345, s.getId(), entitymanager);
			
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			e = ServiceEmployee.insertEmployee("ZNNRTT80A41D286F", "Orietta", "Zanin", 7, 12, 1970, 35, 1345, boss.getCf(), s.getId(), entitymanager);
			e1 = ServiceEmployee.insertEmployee("ZTTNCL80A01D286G", "Nicola", "Zotta", 5, 10, 1990, 35, 1345,boss.getCf(), s.getId(), entitymanager);
			e2 = ServiceEmployee.insertEmployee("CSPNZE80A41D286R", "Enza", "Caspani", 10, 6, 1991, 35, 1345, boss1.getCf(), s.getId(), entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceEmployee.searchByBoss(boss, entitymanager);
			result1 = ServiceEmployee.searchByBoss(boss.getCf(), entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(e)); 
		assert(result.contains(e1)); 
		assertFalse(result.contains(e2));
		assert(result1.contains(e)); 
		assert(result1.contains(e1)); 
		assertFalse(result1.contains(e2)); 
	}
	
	@Test
	public void  test13_SearchEmployeeBySalary() {
		Employee e = null;
		Employee e1 = null;
		Employee e2 = null;
		Collection<Employee> result = new ArrayList<Employee>();
		try {
			e = ServiceEmployee.insertEmployee("PLCMTT80A01E063Y", "Matteo", "Pelucchi", 7, 12, 1970, 35, 1300, entitymanager);
			e1 = ServiceEmployee.insertEmployee("PLCPLA80A01D286L", "Paolo", "Pelucchi", 5, 10, 1990, 42, 1645, entitymanager);
			e2 = ServiceEmployee.insertEmployee("ZTTLRT80A01D286P", "Alberto", "Zotta", 10, 6, 1991, 24, 780, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceEmployee.searchBySalary(1000, 1700, entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(e)); 
		assert(result.contains(e1)); 
		assertFalse(result.contains(e2)); 
	}
	
	@Test
	public void test14_FailCreateEmployeeAlreadyExisting() {
		Employee s = null;
		try {
			s = ServiceEmployee.insertEmployee("ZTTLRT80A01D286P", "Alberto", "Zotta", 10, 6, 1991, 24, 780, entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(AlreadyExistException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}


	@Test
	public void test15_FailNotValidName() {
		Employee s = null;
		try {
			s = ServiceEmployee.insertEmployee("ZTTFRC80A01E063F","", "Zotta", 10, 6, 1991, 24, 780, entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(NotValidException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		entitymanager.close();
    	emfactory.close();
	}
	
}
