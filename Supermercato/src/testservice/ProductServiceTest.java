package testservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Product;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.ProductOperations;
import services.ServiceProduct;
import services.ServiceWarehouse;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServiceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Product> product;
	private static Collection<Warehouse> warehouses;

	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		product = new ArrayList <Product>();
		warehouses = new ArrayList <Warehouse>();
		System.out.println(" ");
		System.out.println("Start Product service test");
	}
		
	@Test
	public void  test01_InsertProductOnlyNameAndWeight() {
		Product p = null;
		try {
			p = ServiceProduct.insertProduct(1, "Pere", 10, entitymanager);
			product.add(p);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(p == null) fail();
		Product found = null;
		try {
			p = ProductOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceProduct.findProduct(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getId()==1);
		assert(p.equals(found));
	}
	
	@Test
	public void  test02_InsertProductOnlyNameAndWeightAndDescription() {
		Product p = null;
		try {
			p = ServiceProduct.insertProduct(2, "Mele", "Golden", 3 ,entitymanager);
			product.add(p);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(p == null) fail();
		Product found = null;
		try {
			p = ProductOperations.findOccurence(2, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceProduct.findProduct(2, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getId()== 2);
		assert(p.equals(found));
	}
	
	@Test
	public void  test03_InsertProductOnlyNameAndWeightAndWarehouse() {
		Product p = null;
		Warehouse w = null;
		Warehouse w1 = null;
		try {
			w = ServiceWarehouse.insertWarehouse(1, "Magazzino spezie", "Bergamo", entitymanager);
			warehouses.add(w);
			w1 = ServiceWarehouse.insertWarehouse(2, "Magazzino 2", "Milano", entitymanager);
			warehouses.add(w1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			p = ServiceProduct.insertProduct(3, "Caff�", 3, warehouses, entitymanager);
			product.add(p);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(p == null) fail();
		Product found = null;
		try {
			p = ProductOperations.findOccurence(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceProduct.findProduct(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getWarehouses() == warehouses);
		p.getWarehouses().forEach((temp) -> {
			try {
				Product sup = ProductOperations.findOccurence(3, entitymanager);
				assert(temp.getProducts().contains(sup));
			} catch(Exception ex) {
				fail(ex.getMessage());
			}
		});
		assert(p.equals(found));
	}
	
	@Test
	public void test04_SuccesfullUpdateNoRelations() {
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Mele");
		m.put("description", "Renette");
		m.put("weight", 22);
		
		try {
			assert(ServiceProduct.updateProduct(2, m, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Product p = null;
		try {
			p = ServiceProduct.findProduct(2, entitymanager);
		}
		catch(Exception e) {
			e.getMessage();
		}
		assert(p.getName() == "Mele");
		assert(p.getDescription() == "Renette");
		assert(p.getWeight() == 22);
	}
	
	@Test
	public void test05_SuccesfullUpdateWithWarehouses() {
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Mele");
		m.put("description", "Renoir");
		m.put("weight", 26);
		Warehouse w = null;
		Warehouse w1 = null;
		warehouses.clear();
		try {
			w = ServiceWarehouse.insertWarehouse(3, "Magazzino 3", "Bergamo", entitymanager);
			warehouses.add(w);
			w1 = ServiceWarehouse.insertWarehouse(4, "Magazzino 4", "Pavia", entitymanager);
			warehouses.add(w1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceProduct.updateProduct(3, m, warehouses, entitymanager));
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		Product p = null;
		try {
			p = ServiceProduct.findProduct(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(p.getName() == "Mele");
		assert(p.getDescription() == "Renoir");
		assert(p.getWeight() == 26);
		assert(p.getWarehouses().equals(warehouses));
	}
	
	@Test
	public void test06_SuccesfullDeleteProduct() {
		assert(ServiceProduct.deleteProduct(1, entitymanager));
		try {
			ServiceProduct.findProduct(1, entitymanager);
		}
		catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void  test07_SearchProductByName() {
		Product p = null;
		Product p1 = null;
		Product p2 = null;
		Collection<Product> result = new ArrayList<Product>();
		try {
			p = ServiceProduct.insertProduct(5, "Mele", "Golden", 10, entitymanager);
			p1 = ServiceProduct.insertProduct(6, "Mele", "Renetta", 15, entitymanager);
			p2 = ServiceProduct.insertProduct(7, "Pere", "Williams", 1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceProduct.searchByName("Mele", entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(p)); 
		assert(result.contains(p1)); 
		assertFalse(result.contains(p2)); 
	}
	
	@Test
	public void  test08_SearchProductByWeight() {
		Product p = null;
		Product p1 = null;
		Product p2 = null;
		Collection<Product> result = new ArrayList<Product>();
		try {
			p = ServiceProduct.insertProduct(8, "Fragole", 10, entitymanager);
			p1 = ServiceProduct.insertProduct(9, "Banane", 15, entitymanager);
			p2 = ServiceProduct.insertProduct(10, "Albicocche", 1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceProduct.searchByWeight(9, 18, entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(p)); 
		assert(result.contains(p1)); 
		assertFalse(result.contains(p2)); 
	}
	
	@Test
	public void  test09_SearchProductByWarehouse() {
		Product p = null;
		Product p1 = null;
		Product p2 = null;
		Warehouse w = null;
		Warehouse w1 = null;
		Warehouse w2 = null;
		warehouses.clear();
		Collection<Product> result = new ArrayList<Product>();
		Collection<Warehouse> ware = new ArrayList<Warehouse>();
		try {
			w = ServiceWarehouse.insertWarehouse(8, "Magazzino 5", "Bergamo", entitymanager);
			warehouses.add(w);
			w1 = ServiceWarehouse.insertWarehouse(9, "Magazzino 6", "Alassio", entitymanager);
			warehouses.add(w1);
			w2 = ServiceWarehouse.insertWarehouse(10, "Magazzino 7", "Empoli", entitymanager);
			ware.add(w2);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			p = ServiceProduct.insertProduct(11, "Cachi", 10, warehouses, entitymanager);
			p1 = ServiceProduct.insertProduct(12, "Carote", 15, warehouses, entitymanager);
			p2 = ServiceProduct.insertProduct(13, "Cipolle", 1, ware, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceProduct.searchByWarehouse(w, entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(p)); 
		assert(result.contains(p1)); 
		assertFalse(result.contains(p2)); 
	}
	
	@Test
	public void test10_FailCreateProductAlreadyExisting() {
		Product s = null;
		try {
			s = ServiceProduct.insertProduct(2, "Mele", "Golden", 3 ,entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(AlreadyExistException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}


	@Test
	public void test11_FailNotValidName() {
		Product s = null;
		try {
			s = ServiceProduct.insertProduct(2, "", "Golden", 3 ,entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(NotValidException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void test12_FailUpdateProduct() {
		HashMap<String,Object> m = new HashMap<String, Object>();
		m.put("name", "Mele");
		m.put("description", "Golden");
		m.put("weight", 7);
		try {
			assertFalse(ServiceProduct.updateProduct(4, m, entitymanager));
		} catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		entitymanager.close();
    	emfactory.close();
	}
	
}
