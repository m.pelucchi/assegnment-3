package testservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Employee;
import entities.Supermarket;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.SupermarketOperations;
import services.ServiceEmployee;
import services.ServiceSupermarket;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SupermarketServiceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Supermarket> supermarket;
	private static Collection<Employee> employees;

	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		supermarket = new ArrayList <Supermarket>();
		employees = new ArrayList <Employee>();
		System.out.println(" ");
		System.out.println("Start Supermarket service test");
	}
		
	@Test
	public void  test01_InsertSupermarketOnlyNameAndCity() {
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(1, "Esselunga", "Milano", entitymanager);
			supermarket.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(s == null) fail();
		Supermarket found = null;
		try {
			s = SupermarketOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceSupermarket.findSupermarket(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getId()==1);
		assert(s.equals(found));
	}
	
	@Test
	public void  test02_InsertSupermarketOnlyNameAndCityAndTypology() {
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "Essecorta", "Lucca", "Superstore",entitymanager);
			supermarket.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(s == null) fail();
		Supermarket found = null;
		try {
			s = SupermarketOperations.findOccurence(2, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceSupermarket.findSupermarket(2, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getId()== 2);
		assert(s.equals(found));
	}
	
	@Test
	public void  test03_InsertSupermarketOnlyNameAndCityAndTypologyAndEmployees() {
		Supermarket s = null;
		Employee e = null;
		Employee e1 = null;
		try {
			e = ServiceEmployee.insertEmployee("PLZMTN80A41D286B", "Martina", "Pelo", 1, 1, 1980, entitymanager);
			employees.add(e);
			e1 = ServiceEmployee.insertEmployee("ZLOGFR80T01B354G", "Zola", "Gianfranco", 1, 12, 1980, entitymanager);
			employees.add(e1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			s = ServiceSupermarket.insertSupermarket(3, "Lidl", "Piacenza", "Iperstore", employees, entitymanager);
			supermarket.add(s);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(s == null) fail();
		Supermarket found = null;
		try {
			s = SupermarketOperations.findOccurence(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceSupermarket.findSupermarket(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(found.getEmployees() == employees);
		s.getEmployees().forEach((temp) -> {
			try {
				Supermarket sup = SupermarketOperations.findOccurence(3, entitymanager);
				assert(temp.getSupermarket().equals(sup));
			} catch(Exception ex) {
				fail(ex.getMessage());
			}
		});
		assert(s.equals(found));
	}
	
	@Test
	public void test04_SuccesfullUpdateNoRelations() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "Lidls");
		m.put("city", "Pavia");
		m.put("typology", "Store");
		try {
			assert(ServiceSupermarket.updateSupermarket(1, m, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Supermarket s = null;
		try {
			s = ServiceSupermarket.findSupermarket(1, entitymanager);
		}
		catch(Exception e) {
			e.getMessage();
		}
		assert(s.getName() == "Lidls");
		assert(s.getCity() == "Pavia");
		assert(s.getTypology() == "Store");
	}
	
	@Test
	public void test05_SuccesfullUpdateWithEmployees() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "U2");
		m.put("city", "Bergamo");
		m.put("typology", "Megastore");
		Employee e = null;
		Employee e1 = null;
		try {
			e = ServiceEmployee.insertEmployee("CSSDRD80H15C933C", "Edoardo", "Cassina", 15, 6, 1980, entitymanager);
			employees.add(e);
			e1 = ServiceEmployee.insertEmployee("BNDFNC80A01C933W", "Francesco", "Benedetti", 1, 12, 1980, entitymanager);
			employees.add(e1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceSupermarket.updateSupermarket(3, m, employees, entitymanager));
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		Supermarket s = null;
		try {
			s = ServiceSupermarket.findSupermarket(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(s.getName() == "U2");
		assert(s.getCity() == "Bergamo");
		assert(s.getTypology() == "Megastore");
		assert(s.getEmployees().equals(employees));
	}
	
	@Test
	public void test06_SuccesfullDeleteSupermarket() {
		assert(ServiceSupermarket.deleteSupermarket(1, entitymanager));
		try {
			ServiceSupermarket.findSupermarket(1, entitymanager);
		}
		catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void  test07_SearchSupermarketByCity() {
		Supermarket s = null;
		Supermarket s1 = null;
		Supermarket s2 = null;
		Collection<Supermarket> result = new ArrayList<Supermarket>();
		try {
			s = ServiceSupermarket.insertSupermarket(5, "Emmelunga", "Lucca", entitymanager);
			supermarket.add(s);
			s1 = ServiceSupermarket.insertSupermarket(6, "Prova", "Lucca", entitymanager);
			supermarket.add(s1);
			s2 = ServiceSupermarket.insertSupermarket(7, "Lidlsa", "Lucca", entitymanager);
			supermarket.add(s2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceSupermarket.searchByCity("Lucca", entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(s)); 
		assert(result.contains(s1)); 
		assert(result.contains(s2)); 
	}
	
	@Test
	public void  test08_SearchSupermarketByTypology() {
		Supermarket s = null;
		Supermarket s1 = null;
		Collection<Supermarket> result = new ArrayList<Supermarket>();
		try {
			s = ServiceSupermarket.insertSupermarket(8, "Emmelunga", "Lucca", "Superstore", entitymanager);
			supermarket.add(s);
			s1 = ServiceSupermarket.insertSupermarket(9, "Prova", "Lucca", "Superstore", entitymanager);
			supermarket.add(s1);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceSupermarket.searchByTypology("Superstore", entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(s)); 
		assert(result.contains(s1)); 
	}
	
	@Test
	public void  test09_SearchSupermarketByEmployee() {
		Supermarket s = null;
		Supermarket s1 = null;
		Supermarket s2 = null;
		Employee e = null;
		Employee e1 = null;
		Employee e2 = null;
		Collection<Supermarket> result = new ArrayList<Supermarket>();
		Collection<Employee> emp = new ArrayList<Employee>();
		Collection<Employee> emp1 = new ArrayList<Employee>();
		try {
			e = ServiceEmployee.insertEmployee("PLZMTN98R23D286X", "Martino", "Pelizzo", 23, 10, 1998, entitymanager);
			emp.add(e);
			employees.add(e);
			e1 = ServiceEmployee.insertEmployee("BRGFBA80A01C933M", "Fabio", "Bergo", 1, 12, 1980, entitymanager);
			emp.add(e1);
			employees.add(e1);
			e2 = ServiceEmployee.insertEmployee("BDAPPP80A01A662F", "Pippo", "Baudo", 10, 11, 1996, entitymanager);
			emp1.add(e2);
			employees.add(e2);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			s = ServiceSupermarket.insertSupermarket(10, "Iperal", "Firenza", emp, entitymanager);
			supermarket.add(s);
			s1 = ServiceSupermarket.insertSupermarket(11, "U3", "Pavia", emp1, entitymanager);
			supermarket.add(s1);
			s2 = ServiceSupermarket.insertSupermarket(12, "Despar", "Lucca", emp, entitymanager);
			supermarket.add(s2);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceSupermarket.searchByEmployee(e, entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(s)); 
		assert(result.contains(s2)); 
		assertFalse(result.contains(s1)); 
	}
	
	@Test
	public void test10_FailCreateSupermarketAlreadyExisting() {
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "Essecorta", "Lucca", "Superstore",entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(AlreadyExistException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}


	@Test
	public void test11_FailNotValidName() {
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "", "Lucca", "Superstore",entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(NotValidException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void test12_FailUpdateSupermarket() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "U2");
		m.put("city", "Bergamo");
		m.put("typology", "Megastore");
		try {
			assertFalse(ServiceSupermarket.updateSupermarket(4, m, entitymanager));
		} catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
		entitymanager.close();
    	emfactory.close();
	}
	
}
