package testservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Product;
import entities.Supermarket;
import entities.Warehouse;
import exceptions.AlreadyExistException;
import exceptions.NotFoundException;
import exceptions.NotValidException;
import operations.WarehouseOperations;
import services.ServiceProduct;
import services.ServiceSupermarket;
import services.ServiceWarehouse;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WarehouseServiceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Collection<Warehouse> warehouse;
	private static Collection<Supermarket> supermarkets;
	private static Collection<Product> products;

	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("supermercato");
		entitymanager = emfactory.createEntityManager();
		warehouse = new ArrayList <Warehouse>();
		supermarkets = new ArrayList <Supermarket>();
		products = new ArrayList <Product>();
		System.out.println(" ");
		System.out.println("Start Warehouse service test");
	}
	
	@Test
	public void  test01_InsertWarehouseOnlyNameAndCity() {
		Warehouse w = null;
		try {
			w = ServiceWarehouse.insertWarehouse(1, "Magazzino spezie", "Bergamo", entitymanager);
			warehouse.add(w);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(w == null) fail();
		Warehouse found = null;
		try {
			w = WarehouseOperations.findOccurence(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			found = ServiceWarehouse.findWarehouse(1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getId()==1);
		assert(w.equals(found));
	}
	
	@Test
	public void  test02_InsertWarehouseOnlyNameAndCityAndSupermarket() {
		Warehouse w = null;
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(1, "Esselunga", "Milano", entitymanager);
			supermarkets.add(s);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			w = ServiceWarehouse.insertWarehouse(2, "Magazzino fiori", "Lecco", s, entitymanager);
			warehouse.add(w);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(w == null) fail();
		Warehouse found = null;
		try {
			w = WarehouseOperations.findOccurence(2, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceWarehouse.findWarehouse(2, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(w.getSupermarket()==s);
		assert(w.equals(found));
	}
	
	@Test
	public void  test03_InsertWarehouseOnlyNameAndCityAndProducts() {
		Warehouse w = null;
		Product p = null;
		Product p1 = null;
		try {
			p = ServiceProduct.insertProduct(1, "Pere", 15, entitymanager);
			products.add(p);
			p1 = ServiceProduct.insertProduct(2, "Mele", 12, entitymanager);
			products.add(p1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			w = ServiceWarehouse.insertWarehouse(3, "Magazzino frutta", "Milano", products, entitymanager);
			warehouse.add(w);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		if(w == null) fail();
		Warehouse found = null;
		try {
			w = WarehouseOperations.findOccurence(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			found = ServiceWarehouse.findWarehouse(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(w.getProducts() == products);
		assert(w.equals(found));
		w.getProducts().forEach((temp) -> {
			try {
				Warehouse ware = WarehouseOperations.findOccurence(3, entitymanager);
				assert(temp.getWarehouses().contains(ware));
			} catch(Exception ex) {
				fail(ex.getMessage());
			}
		});
	}
	
	@Test
	public void test04_SuccesfullUpdateNoRelations() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "Magazzino generale");
		m.put("city", "Pavia");
		try {
			assert(ServiceWarehouse.updateWarehouse(1, m, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Warehouse w = null;
		try {
			w = ServiceWarehouse.findWarehouse(1, entitymanager);
		}
		catch(Exception e) {
			e.getMessage();
		}
		assert(w.getName() == "Magazzino generale");
		assert(w.getCity() == "Pavia");
	}
	
	@Test
	public void test05_SuccesfullUpdateWithSupermarket() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "Magazzino2");
		m.put("city", "Lecco");
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "Lidl", "Bari", entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceWarehouse.updateWarehouse(2, m, s, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Warehouse w = null;
		try {
			w = ServiceWarehouse.findWarehouse(2, entitymanager);
		}
		catch(Exception e) {
			e.getMessage();
		}
		assert(w.getName() == "Magazzino2");
		assert(w.getCity() == "Lecco");
		assert(w.getSupermarket().equals(s));
	}
	
	@Test
	public void test06_SuccesfullUpdateWithProduct() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "Magazzino verdura");
		m.put("city", "Milano");
		Product p = null;
		Product p1 = null;
		products.clear();
		try {
			p = ServiceProduct.insertProduct(3, "Lattuga", 3, entitymanager);
			products.add(p);
			p1 = ServiceProduct.insertProduct(4, "Pomodoro", 8, entitymanager);
			products.add(p1);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			assert(ServiceWarehouse.updateWarehouse(3, m, products, entitymanager));
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		Warehouse w = null;
		try {
			w = ServiceWarehouse.findWarehouse(3, entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(w.getName() == "Magazzino verdura");
		assert(w.getCity() == "Milano");
		assert(w.getProducts().contains(p));
		assert(w.getProducts().contains(p1));
	}
	
	@Test
	public void test07_SuccesfullDeleteWarehouse() {
		assert(ServiceWarehouse.deleteWarehouse(1, entitymanager));
		try {
			ServiceWarehouse.findWarehouse(1, entitymanager);
		}
		catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void  test08_SearchWarehouseByCity() {
		Warehouse w = null;
		Warehouse w1 = null;
		Warehouse w2 = null;
		Warehouse w3 = null;
		warehouse.clear();
		Collection<Warehouse> result = new ArrayList<Warehouse>();
		try {
			w = ServiceWarehouse.insertWarehouse(5, "Magazzino 5", "Lucca", entitymanager);
			warehouse.add(w);
			w1 = ServiceWarehouse.insertWarehouse(6, "Magazzino 6", "Lucca", entitymanager);
			warehouse.add(w1);
			w2 = ServiceWarehouse.insertWarehouse(7, "Magazzino 7", "Lucca", entitymanager);
			warehouse.add(w2);
			w3 = ServiceWarehouse.insertWarehouse(8, "Magazzino 8", "Pisa", entitymanager);
			warehouse.add(w3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceWarehouse.searchByCity("Lucca", entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(w)); 
		assert(result.contains(w1)); 
		assert(result.contains(w2)); 
		assertFalse(result.contains(w3)); 
	}
	
	@Test
	public void  test09_SearchSupermarketBySupermarket() {
		Supermarket s = null;
		Supermarket s1 = null;
		Warehouse w = null;
		Warehouse w1 = null;
		Collection<Warehouse> result = new ArrayList<Warehouse>();
		try {
			s = ServiceSupermarket.insertSupermarket(3, "Emmelunga", "Lucca", "Superstore", entitymanager);
			s1 = ServiceSupermarket.insertSupermarket(4, "Lidl", "Bresso", "Iperstore", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			w = ServiceWarehouse.insertWarehouse(9, "Magazzino 9", "Pistoia", s, entitymanager);
			w1 = ServiceWarehouse.insertWarehouse(10, "Magazzino 10", "Firenze", s1, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			result = ServiceWarehouse.searchBySupermarket(s, entitymanager);
		}catch(Exception e) {
			fail(e.getMessage());
		}
		assert(result.contains(w));
		assertFalse(result.contains(w1));
	}
	
	@Test
	public void  test10_SearchWarehousebyProduct() {
		Warehouse w = null;
		Warehouse w1 = null;
		Warehouse w2 = null;
		Product p = null;
		Product p1 = null;
		Product p2 = null;
		Collection<Warehouse> result = new ArrayList<Warehouse>();
		Collection<Product> products1 = new ArrayList<Product>();
		try {
			p = ServiceProduct.insertProduct(5, "Pomodorini", 3, entitymanager);
			products.add(p);
			p1 = ServiceProduct.insertProduct(6, "Zucchine", 8, entitymanager);
			products.add(p1);
			p2 = ServiceProduct.insertProduct(7, "Melanana", 5, entitymanager);
			products1.add(p2);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			w = ServiceWarehouse.insertWarehouse(11, "Magazzino 11", "Lucca", products, entitymanager);
			warehouse.add(w);
			w1 = ServiceWarehouse.insertWarehouse(12, "Magazzino 12", "Milano", products, entitymanager);
			warehouse.add(w1);
			w2 = ServiceWarehouse.insertWarehouse(13, "Magazzino 13", "Monza", products1,  entitymanager);
		}
		catch(Exception ex) {
			fail(ex.getMessage());
		}
		try {
			result = ServiceWarehouse.searchByProduct(p, entitymanager);
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		assert(result.contains(w)); 
		assert(result.contains(w1)); 
		assertFalse(result.contains(w2)); 
	}
	
	@Test
	public void test11_FailCreateWarehouseAlreadyExisting() {
		Supermarket s = null;
		try {
			s = ServiceSupermarket.insertSupermarket(2, "Magazzino fiori", "Lecco",entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(AlreadyExistException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}


	@Test
	public void test12_FailNotValidName() {
		Warehouse s = null;
		try {
			s = ServiceWarehouse.insertWarehouse(13, "", "Lecco",entitymanager);
			if(s != null) {
				fail();
			}
		}
		catch(NotValidException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void test13_FailUpdateWarehouse() {
		HashMap<String,String> m = new HashMap<String, String>();
		m.put("name", "Magazzino error");
		m.put("city", "Bergamo");
		try {
			assertFalse(ServiceWarehouse.updateWarehouse(4, m, entitymanager));
		} catch(NotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@AfterClass
	public static void cleanUpDbCloseConnection() {
    	entitymanager.close();
    	emfactory.close();
	}	
}
